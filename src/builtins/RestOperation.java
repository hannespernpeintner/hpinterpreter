package builtins;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class RestOperation extends BuiltInOperation {
		
		public RestOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {

			
			if (params.size() != 1) {
				throw new InvalidParameterCountException("rest/tail/cdr", params, 2, params.size());
			}

			SchemeExpression evaluatedParam = su.evaluator.evaluate(params.get(0), env);
			if (evaluatedParam instanceof ParsedList) {
			//if (true) {
				debug("rest returns " + ((ParsedList) evaluatedParam).rest().getValue());
				return ((ParsedList) evaluatedParam).rest();
			}
			throw new InvalidDataTypeException("rest", params.get(0).getLineNumber(), params.get(0).getPosition(), "list", params.get(0).getClass().getName());
		}

}
