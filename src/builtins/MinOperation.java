package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class MinOperation extends BuiltInOperation {
		
		public MinOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(su.evaluator.evaluate(schemeExpression, env));
			}
			
			Class<?> c = haveAllCommonDataType(paramsEvaluated);
			
			if (SchemeInt.class.equals(c)) {
				int min = Integer.MAX_VALUE;
				for (SchemeExpression e : paramsEvaluated) {
					if(Integer.parseInt(e.getValue()) < min) {
						min = Integer.parseInt(e.getValue());
					}
				}
				debug("min is " + min);
				return new SchemeInt(String.valueOf(min));
			} else if (SchemeFloat.class.equals(c)) {
				float min = Float.MAX_VALUE;
				for (SchemeExpression e : paramsEvaluated) {
					if(Float.parseFloat(e.getValue()) < min) {
						min = Float.parseFloat(e.getValue());
					}
				}
				debug("min is " + min);
				return new SchemeFloat(String.valueOf(min));
			}
			throw new InvalidDataTypeException("min", params.get(0).getLineNumber(), params.get(0).getPosition(), "int, float", c.getName());
		}

}
