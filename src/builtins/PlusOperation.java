package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeString;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class PlusOperation extends BuiltInOperation {
		
		public PlusOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList rest, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : rest) {
				paramsEvaluated.add(su.evaluate(schemeExpression, env));
			}
			
			Class<?> c = haveAllCommonDataType(paramsEvaluated);
			
			if (SchemeInt.class.equals(c)) {
				int result = 0;
				for (SchemeExpression e : paramsEvaluated) {
					result += Integer.parseInt(e.getValue());
				}
				debug("plus gives " + result);
				return new SchemeInt(String.valueOf(result));
			} else if (SchemeFloat.class.equals(c)) {
				float result = 0;
				for (SchemeExpression e : paramsEvaluated) {
					result += Float.parseFloat(e.getValue());
				}
				debug("plus gives " + result);
				return new SchemeFloat(String.valueOf(result));
			} else if (SchemeString.class.equals(c)) {
				String result = "";
				for (SchemeExpression e : paramsEvaluated) {
					result += e.getValue().replaceAll("\"", "");
				}
				debug("plus gives " + result);
				return new SchemeString(String.valueOf(result));
			}
			throw new InvalidDataTypeException("+", rest.get(0).getLineNumber(), rest.get(0).getPosition(), "int, float", c.getName());
		}
}
