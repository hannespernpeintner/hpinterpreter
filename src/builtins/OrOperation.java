package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeBoolean;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeString;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class OrOperation extends BuiltInOperation {
		
		public OrOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(su.evaluate(schemeExpression, env));
			}
			
//			Class<?> c = haveAllCommonDataType(paramsEvaluated);
			
//			if (SchemeBoolean.class.equals(c)) {
				for (SchemeExpression schemeExpression : paramsEvaluated) {
					if (((SchemeBoolean) schemeExpression).equals(SchemeBoolean.TRUE())) {
						return SchemeList.TRUE();
					}
				}
				return SchemeExpression.FALSE();
//			}
//			throw new InvalidDataTypeException("Or", params.get(0).getLineNumber(), params.get(0).getPosition(), "list", c.getName());
		}

}
