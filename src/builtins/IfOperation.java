package builtins;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeBoolean;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class IfOperation extends BuiltInOperation {
		
		public IfOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {

			
			if (params.size() != 3) {
				throw new InvalidParameterCountException("if", params, 3, params.size());
			}
			
			// TODO: Nicht klammern! Das hat sie auch gesagt. xxx
			if (params.get(0) instanceof ParsedList) {
			//if(true){ 
				SchemeExpression evaluatedParam = su.evaluator.evaluate(params.get(0), env);
				if (((SchemeBoolean) evaluatedParam).equals(SchemeBoolean.FALSE())) {
					debug("if has a false - evaluates " + params.get(2));
					//if (params.get(2) instanceof ParsedList) {
					if (true) {
						return su.evaluator.evaluate(params.get(2), env);	
					} else {
						return params.get(2);
					}
				} else {
					debug("if has a true - evaluates " + params.get(1));
					//if (params.get(2) instanceof ParsedList) {
					if (true) {
						return su.evaluator.evaluate(params.get(1), env);
					} else {
						return params.get(1);
					}
				}
			}
			throw new InvalidDataTypeException("if", params.get(0).getLineNumber(), params.get(0).getPosition(), "true, false", params.get(0).getClass().getName());
		}

}
