package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeBoolean;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeString;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class EqualsOperation extends BuiltInOperation {
		
		public EqualsOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(su.evaluator.evaluate(schemeExpression, env));
			}
			
			if (paramsEvaluated.size() != 2) {
				throw new InvalidParameterCountException("equals", params, 2, paramsEvaluated.size());
			}
			
			Class<?> c = null;
			try {
				c = haveAllCommonDataType(paramsEvaluated);
			} catch (InvalidDataTypeException e) {
				return SchemeExpression.FALSE();
			}
			
			if (SchemeInt.class.equals(c)) {
				int first = Integer.parseInt(paramsEvaluated.get(0).getValue());
				int second = Integer.parseInt(paramsEvaluated.get(1).getValue());
				
				if (first == second) {
					debug(first + " equals " + second);
					return SchemeExpression.TRUE();
				}
				debug(first + " doesn't equal " + second);
				return SchemeExpression.FALSE();
			} else if (SchemeFloat.class.equals(c)) {
				float first = Float.parseFloat(paramsEvaluated.get(0).getValue());
				float second = Float.parseFloat(paramsEvaluated.get(1).getValue());
				
				if (first == second) {
					debug(first + " equals " + second);
					return SchemeExpression.TRUE();
				}
				debug(first + " doesn't equal " + second);
				return SchemeExpression.FALSE();
			} else if (SchemeString.class.equals(c)) {
				String first = paramsEvaluated.get(0).getValue();
				String second = paramsEvaluated.get(1).getValue();
				
				if (first.equals(second)) {
					debug(first + " equals " + second);
					return SchemeExpression.TRUE();
				}
				debug(first + " doesn't equal " + second);
				return SchemeExpression.FALSE();
			} else if (SchemeList.class.equals(c)) {
				SchemeList first = (SchemeList) paramsEvaluated.get(0);
				SchemeList second = (SchemeList) paramsEvaluated.get(1);
				
				if (first.equals(second)) {
					debug(first + " equals " + second);
					return SchemeExpression.TRUE();
				}
				debug(first + " doesn't equal " + second);
				return SchemeExpression.FALSE();
			} else if (ParsedList.class.equals(c)) {
				ParsedList first = (ParsedList) paramsEvaluated.get(0);
				ParsedList second = (ParsedList) paramsEvaluated.get(1);
				
				if (first.equals(second)) {
					debug(first + " equals " + second);
					return SchemeExpression.TRUE();
				}
				debug(first + " doesn't equal " + second);
				return SchemeExpression.FALSE();
			}
			throw new InvalidDataTypeException("equals", params.get(0).getLineNumber(), params.get(0).getPosition(), "int, float, string", c.getName());
		}

}
