package builtins;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class FirstOperation extends BuiltInOperation {
		
		public FirstOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {

			if (params.size() != 1) {
				throw new InvalidParameterCountException("first/head/car", params, 1, params.size());
			}
			
			SchemeExpression evaluatedParam = su.evaluator.evaluate(params.get(0), env);
			
			if (evaluatedParam instanceof SchemeList) {
				
				debug("first returns " + ((ParsedList) evaluatedParam).first().getValue());
				return ((ParsedList) evaluatedParam).first();
//				return ((ParsedList) evaluatedParam).get(0);
			}
			throw new InvalidDataTypeException("first", params.get(0).getLineNumber(), params.get(0).getPosition(), "list", params.get(0).getClass().getName());
		}

}
