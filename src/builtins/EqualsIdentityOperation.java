package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeBoolean;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeString;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class EqualsIdentityOperation extends BuiltInOperation {
		
	public EqualsIdentityOperation() {
	}

	@Override
	public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		List<SchemeExpression> paramsEvaluated = new ArrayList<>();
		for (SchemeExpression schemeExpression : params) {
			paramsEvaluated.add(su.evaluator.evaluate(schemeExpression, env));
		}
		
		if (paramsEvaluated.size() != 2) {
			throw new InvalidParameterCountException("eq?", params, 2, paramsEvaluated.size());
		}
		
//		Class<?> c = null;
//		try {
//			c = haveAllCommonDataType(paramsEvaluated);
//		} catch (InvalidDataTypeException e) {
//			return SchemeExpression.FALSE();
//		}
		
		if (paramsEvaluated.get(0).equals(paramsEvaluated.get(1))) {
			return SchemeExpression.TRUE();
		} else {
			return SchemeExpression.FALSE();
		}
	}
}
