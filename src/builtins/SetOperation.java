package builtins;

import main.SchemeEvaluator;
import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import datatypes.SchemeSymbol;
import datatypes.SchemeVoid;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class SetOperation extends BuiltInOperation {
		
		public SetOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws InvalidParameterCountException, SymbolException, ReservedKeyWordException {
			
			if (params.size() != 2) {
				throw new InvalidParameterCountException("set!", params, 2, params.size());
			}
			
			if (params.get(0).getValue().matches(SchemeEvaluator._symbol)) {
				SchemeSymbol name = new SchemeSymbol(params.get(0).getValue());
				SchemeExpression definition = su.evaluate(params.get(1), env);
				env.update(name.getValue(), definition);
				debug("set! sets " + name.getValue() + " to " + definition);
				return new SchemeVoid();
				
			}
			throw new InvalidDataTypeException("set!", params.get(0).getLineNumber(), params.get(0).getPosition(), "symbol", "sth. else..");
		}

}
