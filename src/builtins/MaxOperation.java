package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class MaxOperation extends BuiltInOperation {
		
		public MaxOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(su.evaluator.evaluate(schemeExpression, env));
			}
			
			Class<?> c = haveAllCommonDataType(paramsEvaluated);
			
			if (SchemeInt.class.equals(c)) {
				int max = Integer.MIN_VALUE;
				for (SchemeExpression e : paramsEvaluated) {
					if(Integer.parseInt(e.getValue()) > max) {
						max = Integer.parseInt(e.getValue());
					}
				}
				debug("max is " + max);
				return new SchemeInt(String.valueOf(max));
			} else if (SchemeFloat.class.equals(c)) {
				float max = Float.MIN_NORMAL;
				for (SchemeExpression e : paramsEvaluated) {
					if(Float.parseFloat(e.getValue()) > max) {
						max = Float.parseFloat(e.getValue());
					}
				}
				debug("max is " + max);
				return new SchemeFloat(String.valueOf(max));
			}
			throw new InvalidDataTypeException("max", params.get(0).getLineNumber(), params.get(0).getPosition(), "int, float", c.getName());
		}

}
