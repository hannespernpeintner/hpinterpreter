package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class BeginOperation extends BuiltInOperation {
		
		public BeginOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(su.evaluator.evaluate(schemeExpression, env));
			}
			
			SchemeExpression result = paramsEvaluated.get(paramsEvaluated.size()-1);
			debug("begin finished, returning " + result);
			return result;
		}

}
