package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class ListOperation extends BuiltInOperation {
		
		public ListOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(su.evaluator.evaluate(schemeExpression, env));
			}
			
			//haveAllCommonDataType(paramsEvaluated);
			
			SchemeList result = new SchemeList();
			for (SchemeExpression schemeExpression : paramsEvaluated) {
				result.add(schemeExpression);
			}
			debug("created list " + result);
			return result;
		}

}
