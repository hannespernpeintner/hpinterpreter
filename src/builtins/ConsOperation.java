package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class ConsOperation extends BuiltInOperation {

	@Override
	public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		
		List<SchemeExpression> paramsEvaluated = new ArrayList<>();
		for (SchemeExpression schemeExpression : params) {
			paramsEvaluated.add(su.evaluator.evaluate(schemeExpression, env));
		}
		if (paramsEvaluated.size() == 2) {
			if (paramsEvaluated.get(1) instanceof SchemeList) {
				debug("created list with cons");
				return ((SchemeList) paramsEvaluated.get(1)).cons(paramsEvaluated.get(0));
			} else {
				throw new InvalidDataTypeException(paramsEvaluated.get(0).getValue(), paramsEvaluated.get(1).getLineNumber(), paramsEvaluated.get(1).getPosition(), "list", paramsEvaluated.get(1).getClass().getName());
			}
		} else {
			throw new InvalidParameterCountException(paramsEvaluated.get(0).getValue(), paramsEvaluated.get(0), 2, paramsEvaluated.size());
		}
	}
}
