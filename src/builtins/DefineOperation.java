package builtins;

import main.SchemeEvaluator;
import main.SchemeUtility;
import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import datatypes.SchemeProcedure;
import datatypes.SchemeSymbol;
import datatypes.SchemeVoid;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class DefineOperation extends BuiltInOperation {
		
		
		public DefineOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException {
			
			if (params.size() != 2) {
				throw new InvalidParameterCountException("define", params, 2, params.size());
			}
			
			if (params.size() == 2 && params.get(0).getValue().matches(SchemeEvaluator._symbol)) {
				SchemeSymbol name = new SchemeSymbol(params.get(0).getValue());
				SchemeExpression definition = params.get(1);
				SchemeExpression value = SchemeExpression.NIL();
				try {
					value = su.evaluate(definition, env);
					env.define(name.getValue(), value);
					SchemeUtility.getInstance().addAutoCompletion(name.getValue());
					debug("defined " + name.getValue() + " as " + env.lookup(name.getValue(), name));
				} catch (ReservedKeyWordException e) {
					e.printStackTrace();
				} catch (SymbolException e) {
					System.err.println(e);
				}

//				return new SchemeVoid();
				return value;
				
			} else if (params.size() == 2 && params.get(0) instanceof ParsedList && params.get(1) instanceof ParsedList) {
				ParsedList definition = (ParsedList) params.get(0);
				String name = definition.get(0).getValue();
				ParsedList lambdaParameters = definition.rest();
				ParsedList lambdaBody = (ParsedList) params.get(1);
				SchemeProcedure lambda = new SchemeProcedure(lambdaParameters, lambdaBody, new Environment(env));
				try {
					env.define(name, lambda);
					debug("defined " + name + " as lambda " + env.lookup(name, lambdaBody));
				} catch (ReservedKeyWordException | SymbolException e) {
					System.err.println(e);
				}
//				return new SchemeVoid();
				return lambda;
			}
			throw new InvalidDataTypeException("define", params.get(0).getLineNumber(), params.get(0).getPosition(), "symbol, list", params.get(0).getClass().getName());
		}

}
