package builtins;

import java.util.ArrayList;
import java.util.List;

import main.SchemeEvaluator;
import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class UnEqualsOperation extends BuiltInOperation {
		
		private SchemeEvaluator evaluator;
		
		public UnEqualsOperation(SchemeEvaluator evaluator) {
			this.evaluator = evaluator;
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(evaluator.evaluate(schemeExpression, env));
			}
			
			if (paramsEvaluated.size() != 2) {
				throw new InvalidParameterCountException("!=", params, 2, paramsEvaluated.size());
			}
			
			Class<?> c = haveAllCommonDataType(paramsEvaluated);
			
			if (SchemeInt.class.equals(c)) {
				int first = Integer.parseInt(paramsEvaluated.get(0).getValue());
				int second = Integer.parseInt(paramsEvaluated.get(1).getValue());
				
				if (first != second) {
					debug(first + " unequals " + second);
					return SchemeExpression.TRUE();
				}
				return SchemeExpression.FALSE();
			} else if (SchemeFloat.class.equals(c)) {
				float first = Float.parseFloat(paramsEvaluated.get(0).getValue());
				float second = Float.parseFloat(paramsEvaluated.get(1).getValue());
				
				if (first != second) {
					debug(first + " unequals " + second);
					return SchemeExpression.TRUE();
				}
				return SchemeExpression.FALSE();
			}
			throw new InvalidDataTypeException("!=", params.get(0).getLineNumber(), params.get(0).getPosition(), "int, float", c.getClass().getName());
		}

}
