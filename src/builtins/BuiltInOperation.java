package builtins;

import java.util.List;

import main.SchemeUtility;
import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public abstract class BuiltInOperation {
	
	protected SchemeUtility su = SchemeUtility.getInstance();
	
	public abstract SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException;
	
	protected Class<? extends SchemeExpression> haveAllCommonDataType(List<SchemeExpression> results) {
		Class<? extends SchemeExpression> c = results.get(0).getClass();
		
		for (SchemeExpression schemeExpression : results) {
			if (!c.isInstance(schemeExpression)) {
				throw new InvalidDataTypeException("Builtin", 0, 0,  SchemeExpression.class.getName(), c.getClass().getName());
			}
		}
		
		return c;
	}

	public void debug(String input) {
		su.debug(input);
	}
	
}
