package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class LessThanEqualOperation extends BuiltInOperation {
		
		public LessThanEqualOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(su.evaluator.evaluate(schemeExpression, env));
			}
			
			if (paramsEvaluated.size() != 2) {
				throw new InvalidParameterCountException("<=", params, 2, paramsEvaluated.size());
			}
			
			Class<?> c = haveAllCommonDataType(paramsEvaluated);
			
			if (SchemeInt.class.equals(c)) {
				int first = Integer.parseInt(paramsEvaluated.get(0).getValue());
				int second = Integer.parseInt(paramsEvaluated.get(1).getValue());
				
				if (first <= second) {
					debug(first + " <= " + second);
					return SchemeExpression.TRUE();
				}
				debug(first + " is not <= " + second);
				return SchemeExpression.FALSE();
			} else if (SchemeFloat.class.equals(c)) {
				float first = Float.parseFloat(paramsEvaluated.get(0).getValue());
				float second = Float.parseFloat(paramsEvaluated.get(1).getValue());
				
				if (first <= second) {
					debug(first + " <= " + second);
					return SchemeExpression.TRUE();
				}
				debug(first + " is not <= " + second);
				return SchemeExpression.FALSE();
			}
			throw new InvalidDataTypeException("<=", params.get(0).getLineNumber(), params.get(0).getPosition(), "int, float", c.getName());
		}

}
