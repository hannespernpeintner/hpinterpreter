package builtins;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class LengthOperation extends BuiltInOperation {
		
		public LengthOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {

			
			if (params.size() != 1) {
				throw new InvalidParameterCountException("length", params, 1, params.size());
			}
			
			if (su.evaluator.evaluate(params.get(0), env) instanceof SchemeList) {
				SchemeExpression evaluatedParam = su.evaluator.evaluate(params.get(0), env);
				debug("length is " + String.valueOf(((SchemeList) evaluatedParam).size()));
				return new SchemeInt(String.valueOf(((SchemeList) evaluatedParam).size()));
			}
			throw new InvalidDataTypeException("length", params.get(0).getLineNumber(), params.get(0).getPosition(), "list", params.get(0).getClass().getName());
		}

}
