package builtins;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import datatypes.SchemeVoid;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class PrintOperation extends BuiltInOperation {
		
		public PrintOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws InvalidParameterCountException, ReservedKeyWordException {
			String output = "";
			
			SchemeExpression result = su.evaluate(params.get(0), env);
			
			output += result;
			
			System.out.println(">>> " + output.trim());

			debug("print returns " + result);
			return result;
		}

}
