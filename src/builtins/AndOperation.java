package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeBoolean;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeString;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class AndOperation extends BuiltInOperation {
		
		public AndOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(su.evaluate(schemeExpression, env));
			}
			
//			Class<?> c = haveAllCommonDataType(paramsEvaluated);
			
//			if (SchemeList.class.equals(c)) {
				for (SchemeExpression schemeExpression : paramsEvaluated) {
					if (((SchemeBoolean) schemeExpression).equals(SchemeExpression.FALSE())) {
						return SchemeExpression.FALSE();
					}
				}
				return SchemeExpression.TRUE();
//			}
//			throw new InvalidDataTypeException("And", params.get(0).getLineNumber(), params.get(0).getPosition(), "list", c.getName());
		}

}
