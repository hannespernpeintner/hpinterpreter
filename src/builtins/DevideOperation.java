package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class DevideOperation extends BuiltInOperation {
		
		public DevideOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(su.evaluator.evaluate(schemeExpression, env));
			}
			
			Class<?> c = haveAllCommonDataType(paramsEvaluated);
			
			if (SchemeInt.class.equals(c)) {
				int result = Integer.parseInt(paramsEvaluated.get(0).getValue());
				for (int i = 1; i < paramsEvaluated.size(); i++) {
					result /= Integer.parseInt(paramsEvaluated.get(i).getValue());
				}
				debug("division successful, result is " + result);
				return new SchemeInt(String.valueOf(result));
			} else if (SchemeFloat.class.equals(c)) {
				float result = Float.parseFloat(paramsEvaluated.get(0).getValue());
				for (int i = 1; i < paramsEvaluated.size(); i++) {
					result /= Float.parseFloat(paramsEvaluated.get(i).getValue());
				}
				debug("division successful, result is " + result);
				return new SchemeFloat(String.valueOf(result));
			}
			throw new InvalidDataTypeException("devide", params.get(0).getLineNumber(), params.get(0).getPosition(), "int, float", c.getName());
		}

}
