package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class MultiplyOperation extends BuiltInOperation {
		
		public MultiplyOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList params, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : params) {
				paramsEvaluated.add(su.evaluator.evaluate(schemeExpression, env));
			}
			
			Class<? extends SchemeExpression> c = haveAllCommonDataType(paramsEvaluated);
			
			if (SchemeInt.class.equals(c)) {
				int result = 1;
				for (SchemeExpression e : paramsEvaluated) {
					result *= Integer.parseInt(e.getValue());
				}
				debug("multiply gives " + result);
				return new SchemeInt(String.valueOf(result));
			} else if (SchemeFloat.class.equals(c)) {
				float result = 0;
				for (SchemeExpression e : paramsEvaluated) {
					result *= Float.parseFloat(e.getValue());
				}
				debug("multiply gives " + result);
				return new SchemeFloat(String.valueOf(result));
			}
			throw new InvalidDataTypeException("*", params.get(0).getLineNumber(), params.get(0).getPosition(), "int, float", c.getName());
		}

}
