package builtins;

import java.util.ArrayList;
import java.util.List;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeString;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class ModuloOperation extends BuiltInOperation {
		
		public ModuloOperation() {
		}
	
		@Override
		public SchemeExpression process(ParsedList rest, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
			List<SchemeExpression> paramsEvaluated = new ArrayList<>();
			for (SchemeExpression schemeExpression : rest) {
				paramsEvaluated.add(su.evaluate(schemeExpression, env));
			}
			
			Class<?> c = haveAllCommonDataType(paramsEvaluated);
			
			if (SchemeInt.class.equals(c)) {
				int first = Integer.parseInt(paramsEvaluated.get(0).getValue());
				int second = Integer.parseInt(paramsEvaluated.get(1).getValue());
				debug("modulo gives " + (first % second));
				return new SchemeInt(String.valueOf(first % second));
			} else if (SchemeFloat.class.equals(c)) {
				float first = Float.parseFloat(paramsEvaluated.get(0).getValue());
				float second = Float.parseFloat(paramsEvaluated.get(1).getValue());
				debug("modulo gives " + (first % second));
				return new SchemeInt(String.valueOf(first % second));
			}
			throw new InvalidDataTypeException("+", rest.get(0).getLineNumber(), rest.get(0).getPosition(), "int, float", c.getName());
		}
}
