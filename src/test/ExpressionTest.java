package test;

import main.SchemeParser;
import main.SchemeUtility;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import datatypes.ParsedList;
import datatypes.SchemeBoolean;
import datatypes.SchemeExpression;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeSymbol;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;
import exceptions.SyntaxErrorException;

public class ExpressionTest {

	SchemeParser parser = new SchemeParser();
	SchemeUtility util = SchemeUtility.getInstance();

	@Before
	@After
	public void resetEnvironments() {
		util.resetEnvironment();
	}

	@Test
	public void simpleParsing() throws SyntaxErrorException {
		ParsedList result = parser.parseAll("(+ 1 1)");
		Assert.assertTrue(result instanceof ParsedList);
		Assert.assertEquals("+", result.first().getValue());
	}
	
	@Test
	public void booleanParsing() throws SyntaxErrorException, SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpret("(true)");
		Assert.assertTrue(result instanceof SchemeBoolean);
		Assert.assertEquals("true", result.getValue());		
	}
	@Test
	public void multipleExpressionsParsing() throws SyntaxErrorException, SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpret("(begin (+ 1 1) (+ 2 2))");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("4", result.getValue());
	}
	@Test
	public void beginEValuation() throws SyntaxErrorException, SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpret("(begin (define a 1) (+ a 2))");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("3", result.getValue());
	}
	@Test
	public void defineLambdaExpressionsParsing() throws SyntaxErrorException {
		ParsedList defineList = parser.parseAll("(define add (lambda (num1 num2) (+ num1 num2)))");
		
		Assert.assertTrue(defineList instanceof ParsedList);
		Assert.assertEquals("", defineList.getValue());
		Assert.assertEquals("define", defineList.getList().get(0).getValue());
		Assert.assertTrue(defineList.getList().get(2) instanceof ParsedList);
		ParsedList lambdaList = (ParsedList) defineList.getList().get(2);
		Assert.assertEquals("lambda", lambdaList.getList().get(0).getValue());
		Assert.assertTrue(lambdaList.getList().get(1) instanceof ParsedList);

		
	}
	@Test
	public void complexParsing() throws SyntaxErrorException, SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpret("(+ 1 (+ 1 2))");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("4", result.getValue());
	}

	@Test
	public void quoteEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(quote a)");
		Assert.assertEquals("a", result.getValue());
	}
	@Test
	public void quoteSyntacticSugarEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("'(1 2 3)");
		Assert.assertTrue(result instanceof SchemeList);
	}
	@Test
	public void quoteWithBrackets() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(quote #(a b c))");
		Assert.assertEquals("#(a b c)", result.getValue());
	}
	@Test
	public void quoteWithBracketsEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("((quote (+ 1 1)))");
		Assert.assertEquals("2", result.getValue());
	}
	@Test
	public void manyQuotesEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("((quote (quote (quote (+ 1 1)))))");
		Assert.assertEquals("2", result.getValue());
	}
	
	@Test
	public void beginEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(begin (define a 10) (define b 20) (set! b 5) (+ a b))");
		Assert.assertEquals("15", result.getValue());
	}

}
