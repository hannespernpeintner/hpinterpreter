package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ExpressionTest.class, EnvironmentTest.class,
						DatatypesTest.class, ArithmeticOperationsEvaluationTest.class,
						ComparisonOperationsTest.class, ListOperationsTest.class,
						Util.class, test.JSTranspilation.AllTestsSuite.class,
						SaschasMengenTest.class})
public class AllTestsSuite {

}
