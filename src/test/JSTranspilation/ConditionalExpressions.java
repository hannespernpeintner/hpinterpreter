package test.JSTranspilation;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import junit.framework.Assert;
import main.InterpreterGUI;
import main.JSTranspiler;
import main.SchemeParser;

import org.junit.Test;

import datatypes.ParsedList;
import exceptions.InvalidParameterCountException;

public class ConditionalExpressions {

	JSTranspiler t = new JSTranspiler();
	SchemeParser p = new SchemeParser();

	@Test
	public void testSimpleSmallerThan() {
		ParsedList ast = p.parseAll("(< 2 1)");
		String jsResult = "";
		try {
			jsResult = t.transpile(ast);
		} catch (InvalidParameterCountException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("2 < 1", jsResult);
	}
	@Test
	public void testSimpleGreaterThan() {
		ParsedList ast = p.parseAll("(> 2 1)");
		String jsResult = "";
		try {
			jsResult = t.transpile(ast);
		} catch (InvalidParameterCountException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("2 > 1", jsResult);
	}
	@Test
	public void testSimpleIf() {
		ParsedList ast = p.parseAll("(if (< 2 1) (2) (1))");
		String jsResult = "";
		try {
			jsResult = t.transpile(ast);
		} catch (InvalidParameterCountException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("if (2 < 1) {\n2\n} else {\n1\n}", jsResult);
	}
	@Test
	public void testIf() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(if (< 1 0) (define x 10) (define x 20))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("if (1 < 0) {\nvar x = 10;\n\n} else {\nvar x = 20;\n\n}", jsResult);
	}
	@Test
	public void testAnd() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(if (and 1 0) (define x 10) (define x 20))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("if (1 && 0) {\nvar x = 10;\n\n} else {\nvar x = 20;\n\n}", jsResult);
	}
	
}
