package test.JSTranspilation;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import junit.framework.Assert;
import main.InterpreterGUI;
import main.JSTranspiler;
import main.SchemeParser;

import org.junit.Test;

import datatypes.ParsedList;
import exceptions.InvalidParameterCountException;

public class ListExpressions {

	JSTranspiler t = new JSTranspiler();
	SchemeParser p = new SchemeParser();


	@Test
	public void testList() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(list 1 2 3 4)");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("[1, 2, 3, 4]", jsResult);
	}
	@Test
	public void testCons() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(cons (1) (list 2 3 4 10))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("[1, 2, 3, 4, 10]", jsResult);
	}
	@Test
	public void testFirst() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(first (list 2 3 4 10))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("[2, 3, 4, 10][0]", jsResult);
	}
	@Test
	public void testRest() throws InvalidParameterCountException, ScriptException, NoSuchMethodException {
		ParsedList ast = p.parseAll("(rest (list 2 3 4 10))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("[2, 3, 4, 10].slice(1, [2, 3, 4, 10].length - 1)", jsResult);
	}
	@Test
	public void testLength() throws InvalidParameterCountException, ScriptException, NoSuchMethodException {
		ParsedList ast = p.parseAll("(define (x) (length (list 2 3 4 10)))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("var x = [2, 3, 4, 10].length;\n", jsResult);
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
		
		engine.eval(jsResult);
		
		Object obj = engine.get("x");
		Assert.assertEquals("4.0", obj.toString());
	}
}
