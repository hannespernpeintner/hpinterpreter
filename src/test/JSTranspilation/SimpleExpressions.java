package test.JSTranspilation;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import junit.framework.Assert;
import main.InterpreterGUI;
import main.JSTranspiler;
import main.SchemeParser;

import org.junit.Test;

import datatypes.ParsedList;
import exceptions.InvalidParameterCountException;

public class SimpleExpressions {

	JSTranspiler t = new JSTranspiler();
	SchemeParser p = new SchemeParser();

	@Test
	public void testSimpleDefine() {
		ParsedList ast = p.parseAll("(define x 1)");
		String jsResult = "";
		try {
			jsResult = t.transpile(ast);
		} catch (InvalidParameterCountException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("var x = 1;\n", jsResult);
	}
	@Test
	public void testComplex() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(define x (+ 1 1))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("var x = 1 + 1;\n", jsResult);
	}
	@Test
	public void testString() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(\"test\")");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("\"test\"", jsResult);
	}
	@Test
	public void testStringFunction() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(define (foo x y)(\"test\"))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("var foo = function(x, y) {\n\t\"test\"\n};\n", jsResult);
	}
	@Test
	public void testReturn() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(return x)");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("return x\n", jsResult);
	}

	@Test
	public void testBegin() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(begin (define x 10) (define y 20))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("var x = 10;\n\nvar y = 20;\n\n", jsResult);
	}
	@Test
	public void testListDefine() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(define x (list 1 2 3 4))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("var x = [1, 2, 3, 4];\n", jsResult);
	}
	@Test
	public void testLambda() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(lambda (x y) (+ x y))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("function(x, y) {\nreturn x + y\n}", jsResult);
	}
	@Test
	public void testLambdaDefine() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(define bla (lambda (x y) (+ x y)))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("var bla = function(x, y) {\nreturn x + y\n};\n", jsResult);
	}
	@Test
	public void testLambdaShortDefine() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(define (bla x y) (return (+ x y)))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("var bla = function(x, y) {\n\treturn x + y\n\n};\n", jsResult);
	}
	@Test
	public void testLambdaShortWithBeginDefine() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(define (bla x y) (begin (- 2 1) (+ x y)))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("var bla = function(x, y) {\n\t2 - 1\nx + y\n\n};\n", jsResult);
	}
	@Test
	public void testLet() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(let (bla) (10))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("bla = 10;\n", jsResult);
	}
	@Test
	public void testSet() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(set! (bla) (10))");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("bla = 10;\n", jsResult);
	}
	@Test
	public void testJSExecution() throws InvalidParameterCountException, ScriptException, NoSuchMethodException {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
        ParsedList ast = p.parseAll("(define bla (lambda (x y) (+ x y)))");
		String jsResult = "";
		jsResult = t.transpile(ast);

		engine.eval(jsResult);
		
		Object obj = engine.get("bla");
		Invocable inv = (Invocable) engine;
		Object returnValue = inv.invokeFunction("bla", 1, 1);
		Assert.assertEquals("2.0", returnValue.toString());
	}

	@Test
	public void testFunctionCall() throws InvalidParameterCountException {
		ParsedList ast = p.parseAll("(bla x y 10)");
		String jsResult = "";
		jsResult = t.transpile(ast);
		Assert.assertEquals("bla(x, y, 10)", jsResult);
	}
}
