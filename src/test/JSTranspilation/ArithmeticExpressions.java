package test.JSTranspilation;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import junit.framework.Assert;
import main.InterpreterGUI;
import main.JSTranspiler;
import main.SchemeParser;

import org.junit.Test;

import datatypes.ParsedList;
import exceptions.InvalidParameterCountException;

public class ArithmeticExpressions {

	JSTranspiler t = new JSTranspiler();
	SchemeParser p = new SchemeParser();

	@Test
	public void testSimpleAddition() {
		ParsedList ast = p.parseAll("(+ 1 1)");
		String jsResult = "";
		try {
			jsResult = t.transpile(ast);
		} catch (InvalidParameterCountException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("1 + 1", jsResult);
	}
	@Test
	public void testSimpleSubtraction() {
		ParsedList ast = p.parseAll("(- 1 1)");
		String jsResult = "";
		try {
			jsResult = t.transpile(ast);
		} catch (InvalidParameterCountException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("1 - 1", jsResult);
	}
	@Test
	public void testSimpleMultiplication() {
		ParsedList ast = p.parseAll("(* 1 1)");
		String jsResult = "";
		try {
			jsResult = t.transpile(ast);
		} catch (InvalidParameterCountException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("1 * 1", jsResult);
	}
	@Test
	public void testSimpleDivision() {
		ParsedList ast = p.parseAll("(/ 1 1)");
		String jsResult = "";
		try {
			jsResult = t.transpile(ast);
		} catch (InvalidParameterCountException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("1 / 1", jsResult);
	}
}
