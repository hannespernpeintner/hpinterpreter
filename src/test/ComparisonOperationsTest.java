package test;

import junit.framework.Assert;

import main.SchemeEvaluator;
import main.SchemeParser;
import main.SchemeUtility;

import org.junit.Test;

import datatypes.Environment;
import datatypes.SchemeBoolean;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class ComparisonOperationsTest {

	SchemeUtility util = SchemeUtility.getInstance();

	@Test
	public void simpleLessThanEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(< 1 2)");
		Assert.assertEquals(result, SchemeExpression.TRUE());
	}
	@Test(expected=InvalidParameterCountException.class)
	public void lessThanException() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		util.interpretAndReset("(< 1 2 2)");
	}

	@Test
	public void simpleGreaterThanEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(> 1 2)");
		Assert.assertEquals(result, SchemeExpression.FALSE());
	}
	@Test
	public void simpleGreaterThanEqualEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(>= 2 2)");
		Assert.assertEquals(result, SchemeExpression.TRUE());
	}
	@Test
	public void simpleIntEqualsEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(= 2 2)");
		Assert.assertEquals(result, SchemeExpression.TRUE());
	}
	@Test
	public void simpleFloatEqualsEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(= 2.1 2.2)");
		Assert.assertEquals(result, SchemeExpression.FALSE());
	}
	@Test
	public void simpleStringEqualsEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(= \"test\" \"test\")");
		Assert.assertEquals(result, SchemeExpression.TRUE());
	}
	@Test
	public void simpleUnEqualsEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(!= 1 2)");
		Assert.assertEquals(result, SchemeExpression.TRUE());
	}
	@Test
	public void simpleMaxEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(max 1 2 5 2 2 8 1)");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("8", result.getValue());
	}
	@Test
	public void simpleMinEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(min 1.1 2.1 5.5 2.5 2.2 8.1 1.2345345)");
		Assert.assertTrue(result instanceof SchemeFloat);
		Assert.assertEquals("1.1", result.getValue());
	}
	@Test
	public void simpleIfEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(if (< 10 11) 1 2)");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("1", result.getValue());
	}
	@Test
	public void andEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(if (and (= 1 1) (= 2 2)) 1 2)");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("1", result.getValue());
	}
	@Test
	public void orEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(if (or (= 1 1) (= 1 2)) 1 2)");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("1", result.getValue());
	}
	@Test
	public void orEvaluationWithEvaluatedResult() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(if (or (= 1 1) (= 1 2)) (1) (2))");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("1", result.getValue());
	}
	@Test
	public void identityEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		util.interpret("(define a 2)");
		SchemeExpression result = util.interpret("(eq? a a)");
		Assert.assertEquals(SchemeExpression.TRUE(), result);
	}
	@Test
	public void identityEvaluation2() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		util.interpret("(define a 2)");
		util.interpret("(define b 2)");
		SchemeExpression result = util.interpret("(eq? a b)");
		Assert.assertEquals(result, SchemeExpression.FALSE());
	}
}
