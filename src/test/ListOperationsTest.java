package test;

import junit.framework.Assert;

import main.SchemeEvaluator;
import main.SchemeParser;
import main.SchemeUtility;

import org.junit.Ignore;
import org.junit.Test;

import datatypes.Environment;
import datatypes.SchemeExpression;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class ListOperationsTest {

//	SchemeParser parser = new SchemeParser();
//	SchemeEvaluator evaluator = new SchemeEvaluator();
	SchemeUtility util = SchemeUtility.getInstance();
	
	@Test
	public void buildList() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(list 4 2 2)");
		Assert.assertTrue(result instanceof SchemeList);
		SchemeList numberList = ((SchemeList) result);
		Assert.assertEquals(3, numberList.size());
		Assert.assertTrue(numberList.get(0) instanceof SchemeInt);
		Assert.assertEquals("4", ((SchemeList) result).get(0).getValue());
	}
	
	@Test
	public void firstEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(first (list 4 2 2))");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("4", result.getValue());
	}

	@Test
	public void restEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(rest (list 4 2 1))");
		Assert.assertTrue(result instanceof SchemeList);
		SchemeList numberList = ((SchemeList) result);
		Assert.assertTrue(numberList instanceof SchemeList);
		Assert.assertEquals(2, numberList.size());
		Assert.assertTrue(numberList.get(0) instanceof SchemeInt);
		Assert.assertEquals("2", ((SchemeList) result).get(0).getValue());
		Assert.assertTrue(numberList.get(1) instanceof SchemeInt);
		Assert.assertEquals("1", ((SchemeList) result).get(1).getValue());
	}
	@Test
	public void lengthEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(length (list 4 2 1))");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("3", result.getValue());
	}
	@Test
	public void consEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(cons 4 (list 2 1))");
		Assert.assertTrue(result instanceof SchemeList);
		Assert.assertEquals(3, ((SchemeList) result).getList().size());
	}
	@Test
	public void consEvaluation2() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(cons 4 (cons 2 (list 1)))");
		Assert.assertTrue(result instanceof SchemeList);
		Assert.assertEquals(3, ((SchemeList) result).getList().size());
	}
	@Test
	public void nilEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(cons 4 (cons 2 (nil)))");
		Assert.assertTrue(result instanceof SchemeList);
		Assert.assertEquals(2, ((SchemeList) result).getList().size());
	}
	@Test
	public void nilEvaluation2() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = SchemeUtility.getInstance().interpretAndReset("(nil)");
		Assert.assertEquals(0, ((SchemeList) result).getList().size());
	}

	@Test
//	@Ignore
	public void quickSortTestVonRaphael() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
//		SchemeUtility.getInstance().setDebug(true);
		SchemeUtility.getInstance().interpret("(define (append2 l1 l2) (if (= l1 nil) (l2) (cons (first l1) (append2 (rest l1) l2))))");
		SchemeUtility.getInstance().interpret("(define (append3 a b c) (append2 a (append2 b c)))");
		SchemeUtility.getInstance().interpret("(define (myfilter pred list) (if (eq? list nil) (nil) (if (pred (first list)) (cons (first list) (myfilter pred (rest list))) (myfilter pred (rest list)))))");

		String quicksortDefine = "(define (quicksort mylist) (begin (if (<= (length mylist) 1) (mylist) (let (pivot) ((first mylist)) " 
				+ "(append3 (quicksort (myfilter (lambda (x) (< x pivot)) mylist)) (myfilter  (lambda (x) (eq? x pivot)) mylist) "
				+ "(quicksort (myfilter  (lambda (x) (> x pivot)) mylist)))))))";
		SchemeUtility.getInstance().interpret(quicksortDefine);
		SchemeExpression result = SchemeUtility.getInstance().interpret("(quicksort (list 4 3 2 1))");
		
		SchemeList expected = new SchemeList();
		expected.add(new SchemeInt("1"));
		expected.add(new SchemeInt("2"));
		expected.add(new SchemeInt("3"));
		expected.add(new SchemeInt("4"));
		Assert.assertEquals(expected.toString(), result.toString());
//		SchemeUtility.getInstance().setDebug(false);
	}

}
