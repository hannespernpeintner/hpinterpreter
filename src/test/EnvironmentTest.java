package test;

import junit.framework.Assert;
import main.SchemeEvaluator;
import main.SchemeParser;
import main.SchemeUtility;

import org.junit.Test;

import datatypes.Environment;
import datatypes.SchemeExpression;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeProcedure;
import datatypes.SchemeVoid;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class EnvironmentTest {


	SchemeUtility util = SchemeUtility.getInstance();
	SchemeParser parser = new SchemeParser();
	SchemeEvaluator evaluator = new SchemeEvaluator();

	@Test
	public void simpleEnvironment() throws SymbolException, ReservedKeyWordException {
		Environment env = new Environment();
		env.define("test", new SchemeExpression());
		env.define("test2", new SchemeExpression());
		Assert.assertTrue(env.contains("test"));
	}
	@Test
	public void nestedEnvironment() throws SymbolException, ReservedKeyWordException, SymbolException {
		Environment parentEnv = new Environment();
		Environment testEnv = new Environment(parentEnv);
		SchemeExpression ex = new SchemeExpression();
		parentEnv.define("test", ex);
		Assert.assertTrue(testEnv.contains("test"));
		Assert.assertNotSame(null, testEnv.lookup("test", ex));
		Assert.assertEquals(ex, testEnv.lookup("test", ex));
	}
	@Test
	public void alreadyDefined() throws SymbolException, ReservedKeyWordException, SymbolException, InvalidParameterCountException {
		util.interpret("(define a 10)");
		util.interpretAndReset("(define a 20)");
	}
	@Test(expected=SymbolException.class)
	public void notDefined() throws SymbolException, ReservedKeyWordException, SymbolException {
		Environment parentEnv = new Environment();
		SchemeExpression ex = new SchemeExpression();
		parentEnv.define("test", ex);
		parentEnv.lookup("testxxx", ex);
	}

	@Test
	public void simpleDefineEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		util.interpret("(define axxx 10)");
		Assert.assertTrue(util.globalEnvironment.contains("axxx"));
		util.resetEnvironment();
	}
	@Test
	public void simpleSetEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpret("(list (define a 10) (set! a 11) (a))");
		Assert.assertTrue(result instanceof SchemeList);
		Assert.assertEquals("11", util.globalEnvironment.lookup("a", null).getValue());
		util.resetEnvironment();
	}
	@Test
	public void lambdaEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression evaluated = util.interpret("(define test (lambda (num1 num2) (+ num1 num2)))");
		Assert.assertTrue(evaluated instanceof SchemeProcedure);
		SchemeExpression resultOfAddition = util.interpret("(test 1 2)");
		SchemeExpression resultOfAddition2 = util.interpret("(test 4.1 2.2)");
		Assert.assertTrue(resultOfAddition instanceof SchemeInt);
		Assert.assertEquals("3",  resultOfAddition.getValue());
		Assert.assertEquals("6.3", resultOfAddition2.getValue());
		util.resetEnvironment();
	}
	@Test
	public void lambdaShortSyntaxEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression evaluated = util.interpret("(define (test num1 num2) (+ num1 num2))");
		Assert.assertTrue(evaluated instanceof SchemeProcedure);
		util.globalEnvironment.contains("test");
		SchemeExpression resultOfAddition = util.interpret("(test 1 2)");
		SchemeExpression resultOfAddition2 = util.interpret("(test 4.1 2.2)");
		Assert.assertTrue(resultOfAddition instanceof SchemeInt);
		Assert.assertEquals("3",  resultOfAddition.getValue());
		Assert.assertEquals("6.3", resultOfAddition2.getValue());
		util.resetEnvironment();
	}

	@Test
	public void letEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression evaluated = util.interpretAndReset("(let (x y z) (1 2 3) (* x y z))");
		Assert.assertEquals("6", evaluated.getValue());
	}
	@Test
	public void letEvaluation2() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression evaluated = util.interpretAndReset("(let (x y z) ((+ 1 1) 3 4) (* (+ x x) y z))");
		Assert.assertEquals("48", evaluated.getValue());
	}
	@Test
	public void complexEnvironmentLoops() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		util.interpret("(define (times count) (if (!= count 0) ((times (- count 1))) ((print count))))");
		util.interpretAndReset("(times 5)");
	}

	@Test
	public void tripleClosure() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		util.interpret("(define closure (lambda (a) (lambda (b) (lambda (c) (+ a b c)))))");
		SchemeExpression evaluated = util.interpretAndReset("(((closure 5) 5) 5)");
		Assert.assertTrue(evaluated instanceof SchemeInt);
		Assert.assertEquals("15", evaluated.getValue());
	}
	

	@Test
	public void setEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		util.interpret("(define a 10)");
		util.interpret("(set! a 20)");
		SchemeExpression a = util.interpret("(a)");
		Assert.assertEquals("20", a.getValue());
		util.resetEnvironment();
	}
}
