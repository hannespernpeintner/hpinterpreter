package test;

import junit.framework.Assert;

import main.SchemeUtility;

import org.junit.Test;

import datatypes.SchemeExpression;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class SaschasMengenTest {

	SchemeUtility util = SchemeUtility.getInstance();

	@Test
	public void simpleSet() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		util.resetEnvironment();
		String defineSingletonSet ="(define (singletonSet x) (lambda (y) (= y x)))";
		String defineContains ="(define (contains set_ y) (set_ y))";
		String defineTestSets = "(begin " +
                "(define s1 (singletonSet 1)) " +
                "(define s2 (singletonSet 2)) " +
                "(define s3 (lambda (x) (and (>= x 5) (<= x 15))))" +
                "(define s4 (lambda (x) (and (<= x -5) (>= x -15))))" +
              ")";
		
		String test1 = "(contains s1 1)";
		String test2 = "(contains s2 2)";
		String test3 = "(contains s3 5)";
		String test4 = "(contains s4 -5)";
		String test5 = "(contains s4 -22)";

		util.interpret(defineSingletonSet);
		util.interpret(defineContains);
		util.interpret(defineTestSets);

		Assert.assertEquals(SchemeExpression.TRUE(), util.interpret(test1));
		Assert.assertEquals(SchemeExpression.TRUE(), util.interpret(test2));
		Assert.assertEquals(SchemeExpression.TRUE(), util.interpret(test3));
		Assert.assertEquals(SchemeExpression.TRUE(), util.interpret(test4));
		Assert.assertEquals(SchemeExpression.FALSE(), util.interpret(test5));
		util.resetEnvironment();
	}
	
	@Test
	public void complexSet() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		util.resetEnvironment();
		String defineSingletonSet ="(define (singletonSet x) (lambda (y) (= y x)))";
		String defineContains ="(define (contains set_ y) (set_ y))";
		String defineUnion ="(define (union set1 set2) (lambda (x) (or (contains set1 x) (contains set2 x))))";
		String defineIntersect ="(define (intersect set1 set2) (lambda (x) (and (contains set1 x) (contains set2 x))))";
		String defineTestSets = "(begin" +
				"(define s1 (singletonSet 1)) " +
                "(define s2 (singletonSet 2)) " +
                "(define s3 (lambda (x) (and (>= x 5) (<= x 15))))" +
                "(define s4 (lambda (x) (and (<= x -5) (>= x -15))))" +
                "(define s5 (union s1 s2))" +
                "(define s6 (union s3 s4))" +
                "(define s7 (union s1 s3)))";
		
		String test1 = "(contains s5 1)";
		String test2 = "(contains s5 2)";
		String test3 = "(contains s6 -15)";
		String test4 = "(contains s6 15)";
		String test5 = "(contains s7 44)";

		util.interpret(defineSingletonSet);
		util.interpret(defineContains);
		util.interpret(defineUnion);
		util.interpret(defineIntersect);
		util.interpret(defineTestSets);

		Assert.assertEquals(SchemeExpression.TRUE(), util.interpret(test1));
		Assert.assertEquals(SchemeExpression.TRUE(), util.interpret(test2));
		Assert.assertEquals(SchemeExpression.TRUE(), util.interpret(test3));
		Assert.assertEquals(SchemeExpression.TRUE(), util.interpret(test4));
		Assert.assertEquals(SchemeExpression.FALSE(), util.interpret(test5));
		util.resetEnvironment();
	}

}
