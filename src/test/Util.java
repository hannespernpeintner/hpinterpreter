package test;

import java.util.ArrayList;
import java.util.List;

import main.BracketResult;
import main.SchemeParser;

import org.junit.Assert;
import org.junit.Test;


public class Util {
	SchemeParser parser = new SchemeParser();
	
	@Test
	public void tokenize() {
		List<String> tokens = parser.tokenize("(axc , b)").get(0);
		List<String> splitted = new ArrayList<String>();
		splitted.add("(");
		splitted.add("axc");
		splitted.add(",");
		splitted.add("b");
		splitted.add(")");
		for (int i = 0; i < tokens.size(); i++) {
			Assert.assertEquals(splitted.get(i), tokens.get(i));
		}
	}
	@Test
	public void findCorrespondingBracket() {
		BracketResult position = parser.findCorrespondingBracket("(()())");
		Assert.assertTrue(position.second == 5);
	}
	@Test
	public void findCorrespondingBracket2() {
		BracketResult position = parser.findCorrespondingBracket("(()()()())");
		Assert.assertTrue(position.second == 9);
	}
	@Test
	public void findCorrespondingBracket3() {
		BracketResult position = parser.findCorrespondingBracket("(()()(()())", 11);
		Assert.assertTrue(position.second == 5);
	}

}
