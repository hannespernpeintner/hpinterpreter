package test;

import org.junit.Assert;
import org.junit.Test;

import datatypes.SchemeBoolean;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeString;
import datatypes.SchemeSymbol;

public class DatatypesTest {

	@Test
	public void floatPattern() {
		SchemeFloat _float = new SchemeFloat("44.10");
		Assert.assertTrue(_float.getValue().matches(_float.getPattern()));
		Assert.assertFalse("44".matches(_float.getPattern()));
	}
	@Test
	public void intPattern() {
		SchemeInt _int = new SchemeInt("10");
		Assert.assertTrue(_int.getValue().matches(_int.getPattern()));
		Assert.assertFalse("44.1".matches(_int.getPattern()));
	}
	@Test
	public void intPattern2() {
		SchemeInt _int = new SchemeInt("-10");
		Assert.assertTrue(_int.getValue().matches(_int.getPattern()));
		Assert.assertFalse("--10".matches(_int.getPattern()));
	}
	@Test
	public void stringPattern() {
		SchemeString _string = new SchemeString("\"test\"");
		Assert.assertTrue(_string.getValue().matches(_string.getPattern()));
		Assert.assertFalse("asdf".matches(_string.getPattern()));
	}
	@Test
	public void symbolPattern() {
		SchemeSymbol _symbol = new SchemeSymbol("test");
		Assert.assertTrue(_symbol.getValue().matches(_symbol.getPattern()));
		Assert.assertTrue("$abc".matches(_symbol.getPattern()));
		Assert.assertTrue("_abc".matches(_symbol.getPattern()));
		Assert.assertTrue("abc".matches(_symbol.getPattern()));
		Assert.assertFalse("\"asdf\"".matches(_symbol.getPattern()));
	}
	@Test
	public void booleanPattern() {
		Assert.assertTrue("true".matches(SchemeExpression.TRUE().getPattern()));
		Assert.assertTrue("false".matches(SchemeExpression.TRUE().getPattern()));
		Assert.assertFalse("trueds".matches(SchemeExpression.TRUE().getPattern()));

		Assert.assertFalse((SchemeExpression.TRUE().equals(new SchemeList())));
		Assert.assertFalse((SchemeExpression.FALSE().equals(new SchemeExpression("yxcyxc"))));
		
		SchemeList list = new SchemeList();
		list.add(new SchemeSymbol("asd"));
		Assert.assertFalse((SchemeExpression.FALSE().equals(list)));
		
	}
	
	@Test
	public void toStringTest() {
		Assert.assertEquals("false", SchemeExpression.FALSE().toString());
		Assert.assertEquals("true", SchemeExpression.TRUE().toString());
	}
}
