package test;

import junit.framework.Assert;
import main.SchemeEvaluator;
import main.SchemeParser;
import main.SchemeUtility;

import org.junit.Test;

import datatypes.Environment;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeString;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class ArithmeticOperationsEvaluationTest {

	SchemeUtility util = SchemeUtility.getInstance();

	@Test
	public void simpleIntPlusEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(+ 1 1)");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("2", result.getValue());
	}
	@Test
	public void simpleIntPlusEvaluationNegative() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(+ -1 1)");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("0", result.getValue());
	}
	@Test
	public void simpleFloatPlusEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(+ 1.2 1.2)");
		Assert.assertTrue(result instanceof SchemeFloat);
		Assert.assertEquals("2.4", result.getValue());
	}
	@Test
	public void simpleFloatPlusEvaluationNegative() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(+ -1.2 1.2)");
		Assert.assertTrue(result instanceof SchemeFloat);
		Assert.assertEquals("0.0", result.getValue());
	}
	@Test
	public void simpleFloatMinusEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(- 1.2 1.2)");
		Assert.assertTrue(result instanceof SchemeFloat);
		Assert.assertEquals("0.0", result.getValue());
	}
	@Test
	public void simpleConcatEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(+ \"test\" \"xxx\")");
		Assert.assertTrue(result instanceof SchemeString);
		Assert.assertEquals("testxxx", result.getValue());
	}
	@Test(expected=InvalidDataTypeException.class)
	public void stringMinusException() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		util.interpretAndReset("(- \"test\" \"xxx\")");
	}

	@Test
	public void simpleMultiplyEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(* 2 2 2 2)");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("16", result.getValue());
	}
	@Test
	public void simpleDevideEvaluation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(/ 4 2 2)");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("1", result.getValue());
	}
	@Test
	public void simpleModuloOperation() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(% 4 2)");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("0", result.getValue());
	}
	@Test
	public void simpleModuloOperation2() throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = util.interpretAndReset("(% 15 4)");
		Assert.assertTrue(result instanceof SchemeInt);
		Assert.assertEquals("3", result.getValue());
	}
}
