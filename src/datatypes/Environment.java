package datatypes;

import java.util.HashMap;
import java.util.Map;

import main.SchemeUtility;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class Environment {
	
	private Map<String, SchemeExpression> env = new HashMap<>();
	private Environment parent;
	private SymbolException se = new SymbolException();
	
	public boolean contains(String key) {
		if(env.containsKey(key)) {
			return true;
		} else if (parent != null && parent.env.containsKey(key)) {
			return true;
		}
		
		return false;
	}
	
	public boolean containsInLocal(String key) {
		return env.containsKey(key);
	}
	
	public void define(String key, SchemeExpression value) throws ReservedKeyWordException, SymbolException {

		debug(key + " is defined in" + env);
		boolean isBuiltIn = false;
		if (isBuiltIn) {
			throw new ReservedKeyWordException();
//		} else if (contains(key)) {
		} else if (containsInLocal(key)) {
//			se.setSymbolName(key);
//			se.setLookupType(exceptions.SymbolException.LookupType.Define);
//			se.setLineNumber(value.getLineNumber());
//			throw se;
			env.put(key, value);
		} else {
			env.put(key, value);
		}
	}

	public void update(String key, SchemeExpression value) throws SymbolException {
		if (env.containsKey(key)) {
			env.put(key, value);
			debug(key + " is updated in" + env);
		} else if (parent != null && parent.contains(key)) {
			parent.update(key, value);
		} else {
			se.setSymbolName(key);
			se.setLookupType(exceptions.SymbolException.LookupType.Update);
			se.setLineNumber(value.getLineNumber());
			throw se;
		}
	}
	
	public SchemeExpression lookup(String key, SchemeExpression value) throws SymbolException {
		SchemeExpression result = null;
		
		if (env.containsKey(key)) {
			result = env.get(key);
			debug(key + " is looked up in" + env + " as " + result);
		} else if (parent != null && parent.contains(key)) {
			result = parent.lookup(key, value);
		}
		
		if (null == result) {
			se.setSymbolName(key);
			se.setLookupType(exceptions.SymbolException.LookupType.Lookup);
			se.setLineNumber(value.getLineNumber());
			throw se;
		} else {
			return result;
		}
	}

	public Environment(Environment parent) {
		this.parent = parent;
		
	}

	public Environment() {
	}

	@Override
	public String toString() {
		String string = "";
		java.util.Iterator<String> it = env.keySet().iterator();
		while(it.hasNext()) {
			String key = it.next();
			string +=  key + "=" + env.get(key) + ", ";
		}
		
		return string.trim();
	}
	

	private void debug(String string) {
		SchemeUtility.getInstance().debug(string);
	}

	public Map<String, SchemeExpression> getEnv() {
		return env;
	}

	public void setEnv(Map<String, SchemeExpression> env) {
		this.env = env;
	}

	public Environment getParent() {
		return parent;
	}

	public void setParent(Environment parent) {
		this.parent = parent;
	}
}
