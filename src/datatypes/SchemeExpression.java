package datatypes;

import javax.swing.tree.DefaultMutableTreeNode;

@SuppressWarnings("serial")
public class SchemeExpression extends DefaultMutableTreeNode {

	private final static SchemeList NIL = new SchemeList();
	
	private String value = "";
	private Integer lineNumber = 0;
	private Integer position = 0;

	private static SchemeBoolean TRUEBOOL = new SchemeBoolean("true");
	private static SchemeBoolean FALSEBOOL = new SchemeBoolean();
	
	public SchemeExpression(String value) {
		this(value, new Integer(-1), new Integer(-1));
	}
	
	public SchemeExpression(String value, Integer lineNumber, Integer currentPosition) {
		this.value = value;
		this.setLineNumber(lineNumber);
		this.setPosition(currentPosition);
	}
	protected void setPosition(Integer currentPosition) {
		this.position = currentPosition;
	}

	public Integer getPosition() {
		return position;
	}

	public SchemeExpression() {
	}	
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public String getIdentifier() {
		return "expression";
	}

	@Override
	public String toString() {
		return value;
	}

	public final static SchemeList NIL() {
		return NIL;
	}

	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	public static SchemeBoolean TRUE() {
		return TRUEBOOL;
	}
	public static SchemeBoolean FALSE() {
		return FALSEBOOL;
	}
	
}
