package datatypes;

@SuppressWarnings("serial")
public class SchemeVoid extends SchemeExpression {

	public SchemeVoid(String input) {
	}

	public SchemeVoid() {
		setValue("void");
	}

	@Override
	public String getIdentifier() {
		return "void";
	};
	
	@Override
	public String toString() {
		return getIdentifier();
	}
}
