package datatypes;

@SuppressWarnings("serial")
public class SchemeQuote extends SchemeSymbol {

	private String expressionString;
	private SchemeExpression rest;

	public SchemeQuote(SchemeExpression schemeExpression) {
		this.rest = schemeExpression;
		String expressionString = schemeExpression.getValue();
		if (schemeExpression instanceof SchemeList) {
			for (SchemeExpression toQuote : (SchemeList)schemeExpression) {
				expressionString += toQuote.toString();
			}
		} else if (schemeExpression instanceof ParsedList) {
			for (SchemeExpression toQuote : (ParsedList)schemeExpression) {
				expressionString += toQuote.toString();
			}
		} else {
			expressionString += schemeExpression.getValue();
		}

		
		setExpressionString(expressionString);
	}

	public String getExpressionString() {
		return expressionString;
	}
	
	public SchemeExpression getQuotedExpression() {
		return rest;
	}

	private void setExpressionString(String expressionString) {
		this.expressionString = expressionString;
	}

	@Override
	public String getValue() {
		return expressionString;
	}
}
