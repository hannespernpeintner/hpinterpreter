package datatypes;

public interface BuiltInDatatype {
	
	public String getPattern();
	
}
