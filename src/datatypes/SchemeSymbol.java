package datatypes;

@SuppressWarnings("serial")
public class SchemeSymbol extends SchemeExpression{
	public SchemeSymbol(String string) {
		super(string);
	}

	public SchemeSymbol() {
	}

	@Override
	public String getIdentifier() {
		return "symbol";
	}

	public String getPattern() {
		return "[a-zA-Z_$]+[a-zA-Z_0-9_$]*";
		//return "^(?!list)[a-zA-Z_$]+[a-zA-Z_0-9_$]*";
	}
}
