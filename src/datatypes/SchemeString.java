package datatypes;

@SuppressWarnings("serial")
public class SchemeString extends SchemeExpression implements BuiltInDatatype {

	public SchemeString(String string) {
		super(string);
	}

	public SchemeString() {
	}

	public SchemeString(String token, SchemeExpression ex) {
		super(token);
		this.setLineNumber(ex.getLineNumber());
		this.setPosition(ex.getPosition());
	}

	@Override
	public String getIdentifier() {
		return "string";
	}

	@Override
	public String getPattern() {
		return "\".*\"";
	}
	

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SchemeString) {
			return ((SchemeString) obj).getValue() == getValue();
		}
		return false;
	}
}
