package datatypes;

import java.util.HashMap;
import java.util.Map;

import main.SchemeUtility;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

@SuppressWarnings("serial")
public class SchemeProcedure extends SchemeSymbol implements BuiltInDatatype {

	private ParsedList params = new ParsedList();
	private SchemeExpression body = new ParsedList();
	private Environment env = new Environment();
	private SchemeUtility su = SchemeUtility.getInstance();
	
	public SchemeProcedure(SchemeExpression params, SchemeExpression body, Environment env) {
		this.params = (ParsedList) params;
		this.body =  body;
		this.env = env;
	}
	
	// makes a deep copy of the closure to ensure no state is overriden if a defined closure is referenced more than once
	public SchemeProcedure(SchemeProcedure template) {
		this.params = template.params;
		this.body = template.body;
		this.env = new Environment(template.env.getParent());
	}
	
	@Override
	public String getPattern() {
		return "procedure";
	}

	public SchemeExpression process(ParsedList rest, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		
		if (rest.size() != this.params.size()) {
			throw new InvalidParameterCountException(getPattern(), rest, this.params.size(), rest.size());
		} else {
			for (int i = 0; i < rest.size(); i++) {
				try {
					this.env.define(this.params.get(i).getValue(), su.evaluate(rest.get(i), env));
				} catch (ReservedKeyWordException
						| SymbolException e) {

					this.env.update(this.params.get(i).getValue(), su.evaluate(rest.get(i), env));
				}
			}
		}
		SchemeExpression result = su.evaluate(this.body, this.env);

		return result;
	}
	
	@Override
	public String toString() {
		return params + " - " + body;
	}
	

}
