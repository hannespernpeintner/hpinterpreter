package datatypes;

public class SchemeBoolean extends SchemeExpression implements BuiltInDatatype {

	public SchemeBoolean(String string) {
		setValue(string);
	}

	public SchemeBoolean() {
		setValue("false");
	}

	@Override
	public String getPattern() {
		return "true|false";
	}
}
