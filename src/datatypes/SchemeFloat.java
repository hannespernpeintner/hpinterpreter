package datatypes;

@SuppressWarnings("serial")
public class SchemeFloat extends SchemeExpression implements BuiltInDatatype {
	
	public SchemeFloat(String string, SchemeExpression ex) {
		super(string);
		this.setLineNumber(ex.getLineNumber());
		this.setPosition(ex.getPosition());
	}

	public SchemeFloat() {
	}

	public SchemeFloat(String string) {
		super(string);
	}

	@Override
	public String getIdentifier() {
		return "float";
	}

	@Override
	public String getPattern() {
		return "-?\\d*\\.\\d*";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SchemeFloat) {
			return ((SchemeFloat) obj).getValue() == getValue();
		}
		return false;
	}

}
