package datatypes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import main.Util;

import org.apache.commons.lang3.StringUtils;

import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;


@SuppressWarnings("serial")
public class ParsedList extends SchemeExpression implements Iterable<SchemeExpression>{

	private List<SchemeExpression> list;

	public List<SchemeExpression> getList() {
		return list;
	}
	public ParsedList(String input) {
	}
	
	public ParsedList(List<SchemeExpression> list) {
		this.list = list;
	}

	public ParsedList() {
		list = new ArrayList<SchemeExpression>();
	}
	

	public SchemeExpression first() {
		if (list == null || list.size() == 0) {
			return new ParsedList();
		} else {
			return list.get(0);
		}

//		List<SchemeExpression> copy = new ArrayList<>();
//		copy.add(list.get(0));
//		ParsedList first = new ParsedList(copy);
//		first.setLineNumber(list.get(0).getLineNumber());
//		first.setPosition(list.get(0).getPosition());
//		return first;
	}
	public ParsedList rest() {
		if (list == null || list.size() == 0) {
			ParsedList rest = new ParsedList();
			return rest;
		}
		
		List<SchemeExpression> copy = new ArrayList<>();
		copy.addAll(list);
		ParsedList rest = new ParsedList(copy.subList(1, copy.size()));
		rest.setLineNumber(list.get(0).getLineNumber());
		rest.setPosition(list.get(0).getPosition());
		return rest;
	}
	
	public void add(SchemeExpression ex) {
		list.add(ex);
	}
	
	@Override
	public String getIdentifier() {
		return "list";
	};

//	@Override
	public String toStringxxx() {
		StringBuilder sb = new StringBuilder("(");
		
		if (list == null) {
			return sb.toString();
		}
		
		for (SchemeExpression dt : list) {
			sb.append(dt.toString());
			sb.append(" ");
		}
//		sb.append(")");
		return sb.toString().trim() + ")";
	}
	
	@Override
	public String toString() {
//		return stringRep;
		return Util.join(list, " ", "(", ")");
	}
	
	public String stringRep() {
		return Util.join(list, " ", "(", ")");
	}
	
	@Override
	public Iterator<SchemeExpression> iterator() {
		Iterator<SchemeExpression> it = list.iterator();
		return it;
	}
	
	public SchemeExpression get(int index) throws InvalidParameterCountException {
		if (list.size() >= index) {
			return list.get(index);
		} else {
			throw new InvalidParameterCountException("", this, index, list.size());
		}
	}
	public int size() {
		return list.size();
	}
	
	public ParsedList cons(SchemeExpression ex) {
		ParsedList result = new ParsedList();
		result.add(ex);
		for (SchemeExpression schemeExpression : list) {
			result.add(schemeExpression);
		}
		
		return result;
	}
	
	
}
