package datatypes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import main.Util;

import org.apache.commons.lang3.StringUtils;

import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;


@SuppressWarnings("serial")
public class SchemeList extends ParsedList implements Iterable<SchemeExpression>{

	private List<SchemeExpression> list;

	public List<SchemeExpression> getList() {
		return list;
	}
	public SchemeList(String input) {
	}
	public SchemeList(List<SchemeExpression> list) {
		this.list = list;
	}

	public SchemeList() {
		list = new ArrayList<SchemeExpression>();
	}
	

	public SchemeExpression first() {
		if (list == null || list.size() == 0) {
			return new SchemeList();
		} else {
			return list.get(0);
		}

//		List<SchemeExpression> copy = new ArrayList<>();
//		copy.add(list.get(0));
//		SchemeList first = new SchemeList(copy);
//		first.setLineNumber(list.get(0).getLineNumber());
//		first.setPosition(list.get(0).getPosition());
//		return first;
	}
	public SchemeList rest() {
		if (list == null || list.size() == 0) {
			SchemeList rest = new SchemeList();
			return rest;
		}
		
		List<SchemeExpression> copy = new ArrayList<>();
		copy.addAll(list);
		SchemeList rest = new SchemeList(copy.subList(1, copy.size()));
		rest.setLineNumber(list.get(0).getLineNumber());
		rest.setPosition(list.get(0).getPosition());
		
		return rest;
	}
	
	public void add(SchemeExpression ex) {
		list.add(ex);
	}
	
	@Override
	public String getIdentifier() {
		return "list";
	};

//	@Override
	public String toStringxxx() {
		StringBuilder sb = new StringBuilder("(");
		
		if (list == null) {
			return sb.toString();
		}
		
		for (SchemeExpression dt : list) {
			sb.append(dt.toString());
			sb.append(", ");
		}
//		sb.append(")");
		return sb.toString().trim() + ")";
	}
	@Override
	public String toString() {
		return Util.join(list, " ", "(", ")");
	}
//	
//	@Override
//	public String getValue() {
//		return toString();
//	}
	
	@Override
	public Iterator<SchemeExpression> iterator() {
		Iterator<SchemeExpression> it = list.iterator();
		return it;
	}
	
	public SchemeExpression get(int index) throws InvalidParameterCountException {
		if (list.size() >= index) {
			return list.get(index);
		} else {
			throw new InvalidParameterCountException("", this, index, list.size());
		}
	}
	public int size() {
		return list.size();
	}
	
	public SchemeList cons(SchemeExpression ex) {
		SchemeList result = new SchemeList();
		result.add(ex);
		for (SchemeExpression schemeExpression : list) {
			result.add(schemeExpression);
		}
		
		return result;
	}
	
	@Override
	public boolean equals (Object object) {
		if (object instanceof SchemeList) {
			if (object == SchemeExpression.TRUE() && this.size() >= 1) {
				return true;
			} else if (this.equals(SchemeExpression.TRUE()) && ((SchemeList) object).size() >= 1) {
				return true;
			}

			for (SchemeExpression ex : this.list) {
				if (!ex.getValue().equals(this.getValue())) {
					return false;
				}
			}
			
			return true;
		}
		return false;
	}

	
}
