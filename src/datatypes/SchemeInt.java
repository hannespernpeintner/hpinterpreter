package datatypes;

@SuppressWarnings("serial")
public class SchemeInt extends SchemeExpression implements BuiltInDatatype {

	public SchemeInt(String string, SchemeExpression ex) {
		super(string);
		this.setLineNumber(ex.getLineNumber());
		this.setPosition(ex.getPosition());
	}

	public SchemeInt() {
	}

	public SchemeInt(String valueOf) {
		super(valueOf);
	}

	@Override
	public String getIdentifier() {
		return "int";
	}


	@Override
	public String getPattern() {
		return "-?\\d+";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SchemeInt) {
			return ((SchemeInt) obj).getValue() == getValue();
		}
		return false;
	}
}
