package main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import builtins.AndOperation;
import builtins.BeginOperation;
import builtins.ConsOperation;
import builtins.DefineOperation;
import builtins.DevideOperation;
import builtins.EqualsIdentityOperation;
import builtins.EqualsOperation;
import builtins.FirstOperation;
import builtins.GreaterThanEqualOperation;
import builtins.GreaterThanOperation;
import builtins.IfOperation;
import builtins.LengthOperation;
import builtins.LessThanEqualOperation;
import builtins.LessThanOperation;
import builtins.ListOperation;
import builtins.MaxOperation;
import builtins.MinOperation;
import builtins.MinusOperation;
import builtins.ModuloOperation;
import builtins.MultiplyOperation;
import builtins.OrOperation;
import builtins.PlusOperation;
import builtins.PrintOperation;
import builtins.RestOperation;
import builtins.SetOperation;
import builtins.UnEqualsOperation;
import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeBoolean;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeProcedure;
import datatypes.SchemeQuote;
import datatypes.SchemeString;
import datatypes.SchemeSymbol;
import datatypes.SchemeVoid;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class SchemeEvaluator {
	
	public enum Mode {
		SingleStep,
		Regular
	}
	
	public InterpreterGUI gui;
	public volatile Mode mode = Mode.Regular;
	public volatile boolean stepRequested = true;
	
	java.util.concurrent.CopyOnWriteArrayList<SchemeExpression> stack = new CopyOnWriteArrayList<SchemeExpression>();
	//public List<SchemeExpression> stack = new ArrayList<>();
	public SchemeExpression currentExpression = new SchemeExpression();
	public SchemeExpression currentResult = new SchemeExpression();
	public Environment currentEnvironment = new Environment();
	
	public static String _float = new SchemeFloat().getPattern();
	public static String _int = new SchemeInt().getPattern();
	public static String _symbol = new SchemeSymbol().getPattern();
	public static String _string = new SchemeString().getPattern();

	public void evaluateAll(SchemeExpression ex, Environment env) throws SymbolException, ReservedKeyWordException, InvalidParameterCountException {
		currentExpression = ex;
		currentEnvironment = env;
		currentResult = evaluate(currentExpression, currentEnvironment);
		//return currentResult;
		gui.recreateStackTab();
	}
	
	
	public SchemeExpression evaluate(SchemeExpression ex, Environment env) throws SymbolException, ReservedKeyWordException, InvalidParameterCountException {
		
		requestStep();
		
		String token = ex.getValue();
		addToStack(ex);
		currentExpression = ex;
		currentEnvironment = env;

		
		if (ex instanceof ParsedList && !(ex instanceof SchemeList)) {

			SchemeExpression first = ((ParsedList) ex).first();
			String firstTokens = "";
//			try {
				firstTokens = first.getValue();
//			} catch (Exception e) {
//				return new SchemeList();
//			}
			ParsedList rest = ((ParsedList) ex).rest();

			if (firstTokens.startsWith("+")) {
				return (new PlusOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("-")) {
				return (new MinusOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("*")) {
				return (new MultiplyOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("/")) {
				return (new DevideOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("%")) {
				return (new ModuloOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("<=")) {
				return (new LessThanEqualOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith(">=")) {
				return (new GreaterThanEqualOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("<")) {
				return (new LessThanOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith(">")) {
				return (new GreaterThanOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("!=")) {
				return (new UnEqualsOperation(this)).process(rest, env);
				
			} else if (firstTokens.startsWith("=") || firstTokens.startsWith("equal")) {
				return (new EqualsOperation()).process(rest, env);
				
			}  else if (firstTokens.startsWith("eq?")) {
				return (new EqualsIdentityOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("max")) {
				return (new MaxOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("min")) {
				return (new MinOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("list")) {
				return (new ListOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("begin")) {
				return (new BeginOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("nil")) {
				return SchemeList.NIL();
				
			} else if (firstTokens.startsWith("true")) {
				return SchemeExpression.TRUE();
				
			} else if (firstTokens.startsWith("false")) {
				return SchemeExpression.FALSE();
				
			} else if (firstTokens.startsWith("cons")) {
				return (new ConsOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("first") || firstTokens.startsWith("head") || firstTokens.startsWith("car")) {
				return (new FirstOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("rest") || firstTokens.startsWith("tail") || firstTokens.startsWith("cdr")) {
				return (new RestOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("length")) {
				return (new LengthOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("if")) {
				return (new IfOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("and")) {
				return (new AndOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("or")) {
				return (new OrOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("define")) {
				return (new DefineOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("print")) {
				return (new PrintOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("set!")) {
				return (new SetOperation()).process(rest, env);
				
			} else if (firstTokens.startsWith("lambda")) {
				SchemeExpression lambdaParams = rest.get(0);
				SchemeExpression lambdaBody = rest.get(1);
				SchemeProcedure lambda = new SchemeProcedure(lambdaParams, lambdaBody, new Environment(env));
				return lambda;
				
			} else if (firstTokens.startsWith("let")) {
				if (rest.size() < 2) {
					throw new InvalidParameterCountException(ex.getValue(), ex, 2, rest.size());
				} else {
					ParsedList defines = (ParsedList) rest.get(0);
					ParsedList values = (ParsedList) rest.get(1);
					
					for (int i = 0; i < defines.size(); i++) {
						env.define(defines.get(i).getValue(), evaluate(values.get(i), env));
					}
					if (rest.size() == 2) {
						return new SchemeVoid();
					} else {
						return evaluate(rest.get(2), env);	
					}
				}
				
			} else if (firstTokens.startsWith("quote")) {
				return new SchemeQuote(rest);
				
			} else if (firstTokens == "void") {
				debug(firstTokens + " is void");
				return SchemeList.NIL();
			} else if (firstTokens.matches(_symbol)) {
				debug(firstTokens + " is symbol and looked up");
				SchemeExpression symbol = evaluate(env.lookup(firstTokens, first), env);
				if (symbol instanceof SchemeProcedure) {
					return evaluate((new SchemeProcedure((SchemeProcedure) symbol)).process(rest, env), env);
				}
				return evaluate(symbol, env);
			} else if (firstTokens.matches(_float)) {
				debug(firstTokens + " matches float, is list");
				return new SchemeFloat(firstTokens, ex);
			} else if (firstTokens.matches(_int)) {
				debug(firstTokens + " matches int, is list");
				return new SchemeInt(firstTokens, ex);
			}  else {
				SchemeExpression toEvaluate = evaluate(first, env);

				debug(toEvaluate + " should be evaluated");
				if (toEvaluate instanceof SchemeProcedure) {
					return (new SchemeProcedure((SchemeProcedure) toEvaluate)).process(rest, env);
				}
				return evaluate(toEvaluate, env);
			}
			
		} else if (token.matches(_float)) {
			debug(token + " matches float");
			return new SchemeFloat(token, ex);
		} else if (token.matches(_int)) {
			debug(token + " matches int");
			return new SchemeInt(token, ex);
		}  else if (token.startsWith("nil")) {
			return SchemeList.NIL();
		}	
			else if (token.matches(_string)) {
			debug(token + " matches string");
			return new SchemeString(token, ex);
		} else if (ex instanceof SchemeVoid || token == "void") {
			debug(token + " is void");
			return ex;
		} else if (token.matches(SchemeExpression.TRUE().getPattern())) {
			debug(token + " is boolean");
			if (token == "false") {
				return SchemeExpression.FALSE();
			} else {
				return SchemeExpression.TRUE();
			}
		} else if (token.matches(_symbol)) {
//			debug(env.getEnv().toString());
			debug(token + " matches symbol and is looked up");
			return evaluate(env.lookup(token, ex), env);
			//return evaluate(ex, env);
		} else if (ex instanceof SchemeQuote) {
			debug(token + " is quote");
			return evaluate(((SchemeQuote) ex).getQuotedExpression(), env);
		}
		return ex;
	}

	private void addToStack(SchemeExpression ex) {
		if (SchemeUtility.getInstance().withStack() && !(ex instanceof SchemeVoid)) {
			stack.add(ex);
		}
	}


	private void requestStep() {
		while ((!stepRequested && mode == Mode.SingleStep)) {
			try {
				Thread.sleep(200);
				//System.out.println("waiting");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (mode == Mode.SingleStep) {
			gui.recreateStackTab();
			gui.recreateCurrentEnvironmentTab();
			stepRequested = false;
			gui.threadStateBar.setEnabled(false);
			gui.threadStateBar.setString("Step requested: " + gui.schemeUtility.evaluator.stepRequested);
		} else if (mode == Mode.Regular) {
			stepRequested = true;
		}
	}


	private void debug(String string) {
		SchemeUtility.getInstance().debug(string);
	}
}
