package main;

import java.awt.Color;

public class BracketHighlighter extends TextHighlighter {

	public BracketHighlighter(String pattern, Color color) {
		super(pattern, color);
	}

	public BracketResult getBracketPos(String input, int currentPos) {
		return SchemeUtility.getInstance().getCorrespondingBracket(input, currentPos);
	}	
}
