package main;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import exceptions.SymbolException;

public class EditEnvironmentTableAction extends AbstractAction {

	@Override
	public void actionPerformed(ActionEvent e) {
		TableCellListener tcl = (TableCellListener)e.getSource();
		System.out.println(tcl.getKey());
		try {
			SchemeUtility.getInstance().globalEnvironment.update(tcl.getKey(), SchemeUtility.getInstance().parse("(" + tcl.getNewValue().toString() + ")"));
			SchemeUtility.getInstance().debug("The value was successfully updated from " + tcl.getOldValue() + " to " + tcl.getNewValue());
		} catch (SymbolException e1) {
			System.err.println("The given value cannot be applied! Ensure the value can be interpreted!");
		}
	}
}
