package main;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.alee.managers.tooltip.TooltipManager;

public class Util {
	
	
	public static Icon loadIcon(String name) {

		ImageIcon icon = new ImageIcon("img/" + name, name);
		return (Icon) new ImageIcon(SchemeUtility.getInstance().getClass().getClassLoader().getResource(name));
		//return icon;
	}
	
	public static String loadTextFileAsString(String name) {
		InputStream in = SchemeUtility.getInstance().getClass().getClassLoader().getResourceAsStream(name);
//		InputStream in = this.getClass().getResourceAsStream("/SomeTextFile.txt");
		return convertStreamToString(in);
	}
	
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	static TableModel toTableModel(Map<?,?> map) {
	    DefaultTableModel model = new DefaultTableModel(
	        new Object[] { "Key", "Value" }, 0
	    );
	    for (Map.Entry<?,?> entry : map.entrySet()) {
	        model.addRow(new Object[] { entry.getKey(), entry.getValue() });
	    }
	    return model;
	}
	
	public static String join(Collection collection, String separator, String begin, String end) {
		if (collection == null) {
            return null;
        }
        Iterator iterator = collection.iterator();
        if (iterator == null) {
            return null;
        }
        if (!iterator.hasNext()) {
            return StringUtils.EMPTY;
        }
        	            
	    Object first = iterator.next();
	    if (!iterator.hasNext()) {
	        return ObjectUtils.toString(first);
	    }
	    // two or more elements 
	    StringBuffer buf = 
	        new StringBuffer(256); // Java default is 16, probably too small 
	    if(begin != null) {
	    	buf.append(begin);
	    }
	    if (first != null) {
	        buf.append(first);
	    }
	    while (iterator.hasNext()) {
	        if (separator != null) {
	            buf.append(separator);
	        }
	        Object obj = iterator.next();
	        if (obj != null) {
	            buf.append(obj);
	        }
	    }
	    if(end != null) {
	    	buf.append(end);
	    }
	    return buf.toString();
	}
	
}
