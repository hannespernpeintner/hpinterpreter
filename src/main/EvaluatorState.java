package main;

import datatypes.Environment;
import datatypes.SchemeExpression;

public class EvaluatorState {
	
	SchemeExpression expression;
	Environment environment;
	
	public EvaluatorState(SchemeExpression expression, Environment environment) {
		this.expression = expression;
		this.environment = environment;
	}

}
