package main;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.table.AbstractTableModel;

import datatypes.Environment;
import datatypes.SchemeExpression;

public class EnvironmentTableModel extends AbstractTableModel {
	String[] columnNames = {"Key", "Value"};
	private LinkedHashMap<String, SchemeExpression> env = new LinkedHashMap<String, SchemeExpression>();

	public LinkedHashMap<String, SchemeExpression> getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = new LinkedHashMap<>(env.getEnv());
	}

	public EnvironmentTableModel(InterpreterGUI gui) {
		setEnv(gui.schemeUtility.globalEnvironment);	
	}
	public EnvironmentTableModel(InterpreterGUI gui, Environment env) {
		setEnv(env);
	}

    public String getColumnName(int col) {
        return columnNames[col];
    }
    
	@Override
	public int getColumnCount() {
		if (env.size() > 0) {
			return 0;
		}
		return 2;
	}

	@Override
	public int getRowCount() {
		return env.size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		List vals = new ArrayList<>(env.values());
		List keys = new ArrayList<>(env.keySet());
		if (arg1 == 0) {
			return keys.get(arg0).toString();
		} else {
			return vals.get(arg0).toString();
		}
	}

}
