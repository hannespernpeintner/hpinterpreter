package main;

import java.awt.Dimension;

import javax.swing.JFrame;

import com.alee.laf.label.WebLabel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.tabbedpane.WebTabbedPane;

public class InfoFrame extends JFrame{
	WebTabbedPane tabbedPane = new WebTabbedPane();
	
	public InfoFrame() {
		this.setSize(800,600);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);

		WebScrollPane sp0 = new WebScrollPane(new WebLabel(Util.loadTextFileAsString("interface.html")));
		WebScrollPane sp1 = new WebScrollPane(new WebLabel(Util.loadTextFileAsString("interpreter_commands.html")));
		WebScrollPane sp2 = new WebScrollPane(new WebLabel(Util.loadTextFileAsString("general_info.html")));
		tabbedPane.addTab("General Info", sp2);
		tabbedPane.addTab("Commands", sp1);
		tabbedPane.addTab("Interface", sp0);
        tabbedPane.setPreferredSize ( new Dimension ( 150, 120 ) );
        tabbedPane.setTabPlacement ( WebTabbedPane.LEFT );
        this.add(tabbedPane);
	}
}
