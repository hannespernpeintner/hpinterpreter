package main;

import static main.Util.loadIcon;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.TabbedPaneUI;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.tools.JavaCompiler;

import main.SchemeEvaluator.Mode;

import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import com.alee.extended.panel.WebAccordion;
import com.alee.extended.panel.WebButtonGroup;
import com.alee.extended.statusbar.WebStatusBar;
import com.alee.laf.WebLookAndFeel;
import com.alee.laf.button.WebButton;
import com.alee.laf.button.WebToggleButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.filechooser.WebFileChooser;
import com.alee.laf.label.WebLabel;
import com.alee.laf.progressbar.WebProgressBar;
import com.alee.laf.tabbedpane.WebTabbedPane;
import com.alee.managers.tooltip.TooltipManager;
import com.alee.managers.tooltip.TooltipWay;

import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import exceptions.InvalidDataTypeException;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;
import exceptions.SyntaxErrorException;

public class InterpreterGUI {
	Thread interpreterThread;
	InfoFrame infoFrame = new InfoFrame();
	JFrame mainFrame = new JFrame();
	JPanel mainPanel = new JPanel();
	JSplitPane horizontalPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true);
	JSplitPane horizontalPanel2 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true);
	JSplitPane verticalPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
	JMenuBar menuBar = new JMenuBar();
	JMenu fileMenu = new JMenu("File");
	JMenu codeMenu = new JMenu("Code");
	JMenu consoleMenu = new JMenu("Console");
	JMenuItem fileOpenMenuItem = new JMenuItem("Open");
	ActionListener fileOpenMenuActionListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			

			if (fileChooser.showOpenDialog(mainFrame) == WebFileChooser.APPROVE_OPTION )
            {
                File file = fileChooser.getSelectedFile();
				BufferedReader br;
				try {
					br = new BufferedReader(new FileReader(file.getPath()));
					StringBuilder sb = new StringBuilder();
					String line = br.readLine();
					while (line != null) {
					    sb.append(line);
					    sb.append("\n");
			            line = br.readLine();
					}
					String script = sb.toString();
					editor.setText(script);
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
				}

            }
		}
	};
	JMenuItem parseMenuItem = new JMenuItem("Parse");
	ActionListener parseMenuItemActionListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
	    	ParsedList result = new SchemeList();
	    	result = schemeUtility.parse(getEditorText());
			JTree newAst = schemeUtility.convert(result);
			ast.setModel(newAst.getModel());
			expandAll(ast);
		}
	};
	JMenuItem transpileMenuItem = new JMenuItem("Compile to JavaScript");
	ActionListener transpileMenuItemActionListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
	    	ParsedList result = new SchemeList();
	    	result = schemeUtility.parse(getEditorText());
			JTree newAst = schemeUtility.convert(result);
			ast.setModel(newAst.getModel());
			expandAll(ast);
			JSTranspiler transpiler = new JSTranspiler();
			try {
				javaScriptEditor.setText(transpiler.transpile(result));
			} catch (InvalidParameterCountException e) {
				e.printStackTrace();
			}
		}
	};
	JMenuItem runJavaScriptMenuItem = new JMenuItem("Run JavaScript");
	ActionListener runJavaScriptMenuItemActionListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
			try {
				engine.eval(javaScriptEditor.getText());
			} catch (ScriptException e) {
				e.printStackTrace();
			}
		}
	};
	JMenuItem runMenuItem = new JMenuItem("Run");
	JMenuItem runSelectionMenuItem = new JMenuItem("Run selected code");
	ActionListener runMenuActionListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			interpreterThread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					threadStateBar.setEnabled(true);
					long beginTime = System.currentTimeMillis();
					long endTime = System.currentTimeMillis();
					SchemeExpression result = new SchemeExpression();
					try {
						parseMenuItem.doClick();
						beginTime = System.currentTimeMillis();
						result = schemeUtility.interpret(getEditorText());

					} catch (SymbolException | InvalidParameterCountException | InvalidDataTypeException | ReservedKeyWordException e1) {
						System.err.println(e1.toString());
						return;
					} catch (IndexOutOfBoundsException e2) {
						System.out.println(new SyntaxErrorException("the dark side of the force..", new Integer(0), new Integer(0)).getMessage());
						return;
					} finally {
						endTime = System.currentTimeMillis();
						recreateStackTab();
						recreateGlobalEnvironmentTab();
						recreateCurrentEnvironmentTab();
						editor.removeAllLineHighlights();
						threadStateBar.setEnabled(false);
						threadStateBar.setString("Thread: Finished");
					}
					
					System.out.println(result);
					System.out.println("Interpreted successfully... in " + (endTime-beginTime) + " ms");
				}
			}, "RunAction");
			threadStateBar.setEnabled(false);
			threadStateBar.setString("Step requested: " + schemeUtility.evaluator.stepRequested);
			interpreterThread.start();
		}
	};
	ActionListener runSelectionMenuActionListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			interpreterThread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					threadStateBar.setEnabled(true);
					long beginTime = System.currentTimeMillis();
					long endTime = System.currentTimeMillis();
					SchemeExpression result = new SchemeExpression();
					try {
						parseMenuItem.doClick();
						beginTime = System.currentTimeMillis();
						result = schemeUtility.interpret(getSelectedEditorText());

					} catch (SymbolException | InvalidParameterCountException | InvalidDataTypeException | ReservedKeyWordException e1) {
						System.err.println(e1.toString());
						return;
					} catch (IndexOutOfBoundsException e2) {
						System.out.println(new SyntaxErrorException("the dark side of the force..", new Integer(0), new Integer(0)).getMessage());
						return;
					} finally {
						endTime = System.currentTimeMillis();
						recreateStackTab();
						recreateGlobalEnvironmentTab();
						recreateCurrentEnvironmentTab();
						editor.removeAllLineHighlights();
						threadStateBar.setEnabled(false);
						threadStateBar.setString("Thread: Finished");
					}

					System.out.println(result);
					System.out.println("Interpreted successfully... in " + (endTime-beginTime) + " ms");
				}
			}, "RunSelectionAction");
			threadStateBar.setEnabled(false);
			threadStateBar.setString("Step requested: " + schemeUtility.evaluator.stepRequested);
			interpreterThread.start();
		}
	};
	JMenuItem clearEnvironmentMenuItem = new JMenuItem("Clear Environment");
	ActionListener clearEnvironmentActionListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			schemeUtility.resetEnvironment();
			recreateGlobalEnvironmentTab();
			recreateStackTab();
			tabPane.setSelectedIndex(1);
		}
	};
	JMenuItem clearConsoleMenuItem = new JMenuItem("Clear");
	ActionListener clearConsoleActionListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			console.setText("");
		}
	};
	JCheckBoxMenuItem debugEnabled = new JCheckBoxMenuItem("Debug");
	ItemListener debugItemListener = new ItemListener() {
		
		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == ItemEvent.SELECTED) {
				schemeUtility.setDebug(true);
				System.out.println("Debug enabled...");
			} else {
				schemeUtility.setDebug(false);
				System.out.println("Debug disabled...");
			}
		}
	};
	JCheckBoxMenuItem stackEnabled = new JCheckBoxMenuItem("Stack (slow..)");
	ItemListener stackItemListener = new ItemListener() {
		
		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == ItemEvent.SELECTED) {
				schemeUtility.setWithStack(true);
				System.out.println("Stack enabled...");
			} else {
				schemeUtility.setWithStack(false);
				System.out.println("Stack disabled...");
			}
		}
	};

	WebButton run = new WebButton(loadIcon("Play Green Button.png"));
	WebButton runSelection = new WebButton(loadIcon("Play Blue Button.png"));
	WebButton resume = new WebButton(loadIcon("Play All.png"));
	WebButton stepForward = new WebButton(loadIcon("Play.png"));
	WebToggleButton singleStepModeButton = new WebToggleButton("Singlestep Mode");
	WebToggleButton regularModeButton = new WebToggleButton("Regular Mode");
	WebCheckBox implicitBeginWrapping = new WebCheckBox("Implicit Begin-Wrapping",true);
	WebButton helpButton = new WebButton(loadIcon("Help Blue Button.png"));
//	WebLabel threadStateLabel = new WebLabel("");
	WebProgressBar threadStateBar = new WebProgressBar();

    WebStatusBar currentExpressionStatusBar = new WebStatusBar();
    
	WebLabel currentExpressionLabel = new WebLabel("");
	
	WebButtonGroup modeButtonGroup = new WebButtonGroup(true, singleStepModeButton, regularModeButton);
	RSyntaxTextArea editor = new RSyntaxTextArea();
	RTextScrollPane editorPane = new RTextScrollPane(editor);
	RSyntaxTextArea javaScriptEditor = new RSyntaxTextArea();
	RTextScrollPane javaScriptEditorPane = new RTextScrollPane(javaScriptEditor);
	DefaultCompletionProvider provider = createCompletionProvider();
	AutoCompletion ac = new AutoCompletion(provider);
	JPanel editorMainPane = new JPanel(new GridLayout(1, 0, 10, 10));
	
	DocumentListener editorDocumentListener = new DocumentListener() {
		
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			editor.getHighlighter().removeAllHighlights();
			for (TextHighlighter h : highliters) {
				try {
					String content = getDocument().getText(0, getDocument().getLength());
					int curr = 0;
					while(curr < content.length()) {
						String rest = content.substring(curr);
						
						if(rest.startsWith(h.pattern)) {
							int from = curr;
							int to = curr + h.pattern.length();
							editor.getHighlighter().addHighlight(from, to, h.highliter);
						}
						
						curr++;
					}
				} catch (Exception e) {
				}
			}
		}
		
		@Override
		public void insertUpdate(DocumentEvent arg0) {
			editor.getHighlighter().removeAllHighlights();
			for (TextHighlighter h : highliters) {
				try {
					String content = getDocument().getText(0, getDocument().getLength());
					int curr = 0;
					while(curr < content.length()) {
						String rest = content.substring(curr);
						
						if(rest.startsWith(h.pattern)) {
							int from = curr;
							int to = curr + h.pattern.length();
							editor.getHighlighter().addHighlight(from, to, h.highliter);
						}
						
						curr++;
					}
				} catch (Exception e) {
				}
			}
		}
		
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			
		}
		
	};
	//JScrollPane editorPane = new JScrollPane(editor, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	JTextPane console = new JTextPane();
	JScrollPane consolePane = new JScrollPane(console);
	JTree ast = new JTree(new DefaultMutableTreeNode());
	volatile JTabbedPane tabPane = new JTabbedPane();
	volatile WebAccordion currentInfoArea = new WebAccordion();
	volatile JTabbedPane editorTabPane = new JTabbedPane();
	public volatile JTable globalEnvironmentTable = new JTable();
	public volatile JTable currentEnvironmentTable = new JTable();
	volatile JScrollPane globalEnvironmentPane = new JScrollPane(globalEnvironmentTable);
	volatile JScrollPane currentEnvironmentPane = new JScrollPane(currentEnvironmentTable);
	volatile JTable stackTable = new JTable(new StackTableModel(this));
	volatile JScrollPane stackPane = new JScrollPane(stackTable);
	JScrollPane astPane = new JScrollPane(ast, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	
	WebFileChooser fileChooser = new WebFileChooser("Open");
	List<TextHighlighter> highliters = new ArrayList<>();
	BracketHighlighter bracketHighlighter = new BracketHighlighter(null, new Color(255,165, 0));
	TableCellListener myTablePropertyChangeListener = new TableCellListener(globalEnvironmentTable, new EditEnvironmentTableAction());

	SchemeUtility schemeUtility = SchemeUtility.getInstance();
	
	@SuppressWarnings("serial")
	public InterpreterGUI() {
		schemeUtility.resetEnvironment();
		schemeUtility.evaluator.gui = this;
		ac.install(editor);
		editor.getPopupMenu().add(runSelectionMenuItem);
		JMenuItem tempMenuItem = new JMenuItem("Run selected code");
		tempMenuItem.addActionListener(runSelectionMenuActionListener);
		editor.getPopupMenu().add(tempMenuItem);

		//initializeHighlighters();
		
		mainFrame.setSize(1280, 720);
		mainFrame.setTitle("HPInterpreter for Scheme");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.add(mainPanel);
		mainPanel.setLayout(new BorderLayout(10,10));
		mainPanel.add(menuBar, BorderLayout.PAGE_START);

		menuBar.add(fileMenu);
		menuBar.add(codeMenu);
		menuBar.add(consoleMenu);
		menuBar.add(run);
		menuBar.add(runSelection);
		menuBar.add(resume);
		menuBar.add(stepForward);
		stepForward.setEnabled(false);
		resume.setEnabled(false);
		menuBar.add(modeButtonGroup);
		menuBar.add(implicitBeginWrapping);
		helpButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				infoFrame.setVisible(true);
			}
		});
		menuBar.add(helpButton);
//		menuBar.add(threadStateLabel);
		
		menuBar.add(threadStateBar);
		threadStateBar.setIndeterminate ( true );
		threadStateBar.setStringPainted ( true );
		threadStateBar.setEnabled(false);
		threadStateBar.setString("Thread: Finished");
		
		modeButtonGroup.setButtonsDrawFocus(true);
		regularModeButton.setSelected(true);
		regularModeButton.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == ItemEvent.SELECTED) {
					setEvaluatorMode(Mode.Regular);
				}
			}
		});
		singleStepModeButton.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == ItemEvent.SELECTED) {
					setEvaluatorMode(Mode.SingleStep);
				}
			}
		});
		fileMenu.add(fileOpenMenuItem);
		fileOpenMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		fileOpenMenuItem.addActionListener(fileOpenMenuActionListener);
		fileChooser.setMultiSelectionEnabled(false);

		codeMenu.add(parseMenuItem);
		parseMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.CTRL_MASK));
		parseMenuItem.addActionListener(parseMenuItemActionListener);
		codeMenu.add(runMenuItem);
		codeMenu.add(runSelectionMenuItem);
		runMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		runMenuItem.addActionListener(runMenuActionListener);
		debugEnabled.addItemListener(debugItemListener);
		stackEnabled.addItemListener(stackItemListener);
		consoleMenu.add(debugEnabled);
		consoleMenu.add(stackEnabled);
		consoleMenu.add(clearConsoleMenuItem);
		clearConsoleMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
		clearConsoleMenuItem.addActionListener(clearConsoleActionListener);
		codeMenu.add(clearEnvironmentMenuItem);
		clearEnvironmentMenuItem.addActionListener(clearEnvironmentActionListener);
		
		codeMenu.addSeparator();
		codeMenu.add(transpileMenuItem);
		transpileMenuItem.addActionListener(transpileMenuItemActionListener);
		codeMenu.add(runJavaScriptMenuItem);
		runJavaScriptMenuItem.addActionListener(runJavaScriptMenuItemActionListener);
		

		stepForward.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				schemeUtility.evaluator.stepRequested = true;
				threadStateBar.setEnabled(true);
				threadStateBar.setString("Step requested: " + schemeUtility.evaluator.stepRequested);
				try {
					editor.removeAllLineHighlights();
					editor.addLineHighlight(schemeUtility.evaluator.currentExpression.getLineNumber(), Color.GRAY);
				} catch (BadLocationException e1) {
//					e1.printStackTrace(); doesn't matter..
				}
			}
		});

		run.addActionListener(runMenuActionListener);
		runSelection.addActionListener(runSelectionMenuActionListener);
		resume.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setEvaluatorMode(Mode.Regular);
			}
		});
		TooltipManager.setTooltip(run, "Run");
		TooltipManager.setTooltip(runSelection, "Run selected code");
		TooltipManager.setTooltip(stepForward, "Step Forward");
		TooltipManager.setTooltip(resume, "Resume");
		TooltipManager.setTooltip(helpButton, "Info about everything");
		TooltipManager.setTooltip(singleStepModeButton, "Awaits a step command for every outstanding instruction");
		TooltipManager.setTooltip(regularModeButton, "Executes the script without interruption");
		TooltipManager.setTooltip(editor, "Drop script file here!", TooltipWay.up);
		tabPane.addTab("AST", astPane);
		tabPane.setPreferredSize(new Dimension(400, 400));
		tabPane.addTab("Stack", stackPane);
		tabPane.addTab("Global Env.", globalEnvironmentPane);
		currentInfoArea.addPane("Current Expr.", currentExpressionLabel);
		currentInfoArea.addPane("Current Env.", currentEnvironmentPane);
		recreateGlobalEnvironmentTab();
		recreateStackTab();
		editorPane.setBorder(new LineBorder(Color.black, 1));
		editorPane.setFoldIndicatorEnabled(true);
		editorMainPane.setPreferredSize(new Dimension(400, 400));
		editor.setDropTarget(new DropTarget() {
			public synchronized void drop(DropTargetDropEvent evt) {
		        try {
		            evt.acceptDrop(DnDConstants.ACTION_COPY);
		            @SuppressWarnings("unchecked")
					List<File> droppedFiles = (List<File>) evt.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
		            File file = droppedFiles.get(0);
		            
		            BufferedReader br;
					try {
						br = new BufferedReader(new FileReader(file.getPath()));
						StringBuilder sb = new StringBuilder();
						String line = br.readLine();
						while (line != null) {
						    sb.append(line);
						    sb.append("\n");
				            line = br.readLine();
						}
						String script = sb.toString();
						editor.setText(script);
						br.close();
					} catch (IOException e) {
					}
		            
		        } catch (Exception ex) {
		            ex.printStackTrace();
		        }
		    }
		});
		editorPane.setPreferredSize(new Dimension(400, 400));
		editorTabPane.addTab("Lisp", editorPane);
		editorTabPane.addTab("JavaScript (Beta)",javaScriptEditorPane);
		editorMainPane.add(editorTabPane);
		getDocument().addDocumentListener(editorDocumentListener);
		
		consolePane.setPreferredSize(new Dimension(400, 200));
		consolePane.setBorder(new LineBorder(Color.black, 1));
		DefaultCaret caret = (DefaultCaret)console.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		console.setEditable(false);
		console.setBackground(Color.BLACK);
		console.setForeground(Color.WHITE);
		final Font currFont = consolePane.getFont();
		console.setFont(new Font("Courier New", currFont.getStyle(), currFont.getSize()));
		editor.setFont(new Font("Courier New", currFont.getStyle(), currFont.getSize()));
		editor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_LISP);
		editor.setCodeFoldingEnabled(true);
		editor.setAntiAliasingEnabled(true);
		
		javaScriptEditor.setFont(new Font("Courier New", currFont.getStyle(), currFont.getSize()));
		javaScriptEditor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);

		astPane.setPreferredSize(new Dimension(200, 400));
		astPane.setBorder(new LineBorder(Color.black, 1));

		horizontalPanel2.setDividerLocation(850);
		horizontalPanel.setDividerLocation(850);
		
		verticalPanel.setDividerLocation(400);
		
		mainPanel.add(verticalPanel);
		horizontalPanel.add(editorMainPane);
		horizontalPanel.add(tabPane);
		horizontalPanel2.add(consolePane);
		horizontalPanel2.add(currentInfoArea);
		verticalPanel.add(horizontalPanel);
		verticalPanel.add(horizontalPanel2);
		
		
		redirectSystemStreams();
		mainFrame.setVisible(true);
	}
	
	private DefaultCompletionProvider createCompletionProvider () {
		DefaultCompletionProvider provider = new DefaultCompletionProvider();

		for (String key: SchemeUtility.keywords) {
		    provider.addCompletion(new BasicCompletion(provider, key));
		}
    	return provider;
	}

	private void recreateGlobalEnvironmentTab() {
		String[] columnNames = {"Key", "Value"};
		Map<String, SchemeExpression> env = schemeUtility.globalEnvironment.getEnv();
		List<String> keys = new ArrayList<String>(env.keySet());
		List<SchemeExpression> expressions = new ArrayList<SchemeExpression>(env.values());
		String[][] values = new String[keys.size()][2];
		
		for (int i = 0; i < keys.size(); i++) {
			values[i][0] = keys.get(i);
			values[i][1] = expressions.get(i).toString();
		}
		
		globalEnvironmentTable = new JTable(values, columnNames);
		//globalEnvironmentTable.setModel(dataModel)
		tabPane.remove(globalEnvironmentPane);
		globalEnvironmentPane = new JScrollPane(globalEnvironmentTable);
		tabPane.addTab("Global Env.", globalEnvironmentPane);
		myTablePropertyChangeListener = new TableCellListener(globalEnvironmentTable, new EditEnvironmentTableAction());
	}
	
	public void recreateCurrentEnvironmentTab() {
//		EnvironmentTableModel model = new EnvironmentTableModel(this, schemeUtility.evaluator.currentEnvironment);
		TableModel model = Util.toTableModel(schemeUtility.evaluator.currentEnvironment.getEnv());
		currentEnvironmentTable.setModel(model);
		currentExpressionLabel.setText(schemeUtility.evaluator.currentExpression.toString());
	}
	
	public void recreateStackTab() {
		stackTable.setModel(new StackTableModel(this));
		tabPane.setSelectedComponent(globalEnvironmentPane);
	}
	
	private void redirectSystemStreams() {
		  OutputStream out = new OutputStream() {
			  StyleContext sc = StyleContext.getDefaultStyleContext();
			    javax.swing.text.AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Color.GREEN);

			    
			    @Override
			    public void write(int b) throws IOException {

			    	Document doc = console.getDocument();
			    	try {
						doc.insertString(doc.getLength(), String.valueOf((char) b), aset);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
			    }
			 
			    @Override
			    public void write(byte[] b, int off, int len) throws IOException {
			    	Document doc = console.getDocument();
			    	try {
						doc.insertString(doc.getLength(), new String(b, off, len), aset);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
			    }
			 
			    @Override
			    public void write(byte[] b) throws IOException {
			      write(b, 0, b.length);
			    }
			  };
		  
		  OutputStream outErr = new OutputStream() {
			  StyleContext sc = StyleContext.getDefaultStyleContext();
			    javax.swing.text.AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Color.RED);

			    
			    @Override
			    public void write(int b) throws IOException {
			    	Document doc = console.getDocument();
			    	try {
						doc.insertString(doc.getLength(), String.valueOf((char) b), aset);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
			    }
			 
			    @Override
			    public void write(byte[] b, int off, int len) throws IOException {
			    	Document doc = console.getDocument();
			    	try {
						doc.insertString(doc.getLength(), new String(b, off, len), aset);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
			    }
			 
			    @Override
			    public void write(byte[] b) throws IOException {
			      write(b, 0, b.length);
			    }
			  };
		  		 
		  System.setOut(new PrintStream(out, true));
		  System.setErr(new PrintStream(outErr, true));
		}
	
	private void expandAll(JTree target) {
		for (int i = 0; i < target.getRowCount(); i++) {
			target.expandRow(i);
		}
	}

	public boolean isDebug() {
		return debugEnabled.getState();
	}
	
	public DefaultCompletionProvider getProvider() {
		return provider;
	}
	
	private Document getDocument() {
		return editor.getDocument();
	}

	private String getEditorText() {
		if (implicitBeginWrapping.isSelected()) {
			return "( begin " + editor.getText() + " )";
		}
		return editor.getText();
	}
	private String getSelectedEditorText() {
		return editor.getSelectedText();
	}
	
	private void setEvaluatorMode(Mode target) {
		schemeUtility.evaluator.mode = target;
		if (target == Mode.Regular) {
			resume.setEnabled(false);
			stepForward.setEnabled(false);
			singleStepModeButton.setSelected(false);
			regularModeButton.setSelected(true);
			schemeUtility.evaluator.stepRequested = true;
			threadStateBar.setEnabled(true);
			threadStateBar.setString("Step requested: " + schemeUtility.evaluator.stepRequested);
		} else if (target == Mode.SingleStep) {
			resume.setEnabled(true);
			stepForward.setEnabled(true);
			singleStepModeButton.setSelected(true);
			regularModeButton.setSelected(false);
			schemeUtility.evaluator.stepRequested = false;
			threadStateBar.setEnabled(false);
			threadStateBar.setString("Step requested: " + schemeUtility.evaluator.stepRequested);
		}
	}

public static void main(String[] args) {
	
	WebLookAndFeel.install();
	new InterpreterGUI();
}
}

