package main;

import javax.swing.text.Segment;

import org.fife.ui.rsyntaxtextarea.AbstractTokenMaker;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.TokenMap;

public class MyTokenMaker extends AbstractTokenMaker {

	

	@Override
	public TokenMap getWordsToHighlight() {
		TokenMap tokenMap = new TokenMap();
		
		for(String key: SchemeUtility.getInstance().keywords) {
			   tokenMap.put(key,  Token.RESERVED_WORD);
		}

	    return tokenMap;
	}

	@Override
	public Token getTokenList(Segment arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		return null;
	}

}
