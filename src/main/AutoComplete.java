package main;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JEditorPane;

public class AutoComplete extends AbstractAction {

	private JEditorPane editor;
	private String[] keys;

	public AutoComplete(String[] keys, JEditorPane editor) {
		this.editor = editor;
		this.keys = keys;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		char[] tokens = {' ', ')', '('};
		int lastLimitTokenPosition = findLastOccurence(editor.getText(), tokens);
		String currentToken = "_";
		try {
			if(lastLimitTokenPosition <= 0) {
				lastLimitTokenPosition = 0;
			}
			currentToken = editor.getText().substring(lastLimitTokenPosition, editor.getCaretPosition()).trim();
			if (currentToken.startsWith("(") || currentToken.startsWith(")")) {
				currentToken = currentToken.substring(1);
			}
		} catch (Exception e2) {
			//return;
		}
		List<String> possibles = new ArrayList<>();
		addPossibles(possibles,currentToken);
		
		if (possibles.size() == 1) {
			editor.setText(replaceLast(editor.getText(), currentToken, possibles.get(0)));
		} else if (possibles.size() != 0) {
			String appender = "";
			for (String key: possibles) {
				appender += key + " ";
			}
			System.err.println(possibles.size() + " matches found: " + appender);
		} else {
			System.err.println("No matching builtins found, no autocompletion...");
		}
	}
	
	public static String replaceLast(String string, String toReplace, String replacement) {
	    int pos = string.lastIndexOf(toReplace);
	    if (pos > -1) {
	        return string.substring(0, pos)
	             + replacement
	             + string.substring(pos + toReplace.length(), string.length());
	    } else {
	        return string;
	    }
	}

	public int findLastOccurence(String haystack, char[] needles) {
		int last = 0;
		
		for (char c: needles) {
			int pos = haystack.lastIndexOf(String.valueOf(c));
			if (pos > last) {
				last = pos;
			}
		}
		
		return last;
	}
	
	public Character[] convert(char[] chars) {
        Character[] copy = new Character[chars.length];
        for(int i = 0; i < copy.length; i++) {
            copy[i] = Character.valueOf(chars[i]);
        }
        return copy;
    }

	private void addPossibles(List<String> possibles, String currentToken) {
		for (String key: keys) {
			if (key.startsWith(currentToken)) {
				possibles.add(key);
			}
		}
		
		for(String key: SchemeUtility.getInstance().globalEnvironment.getEnv().keySet()) {
			if (key.startsWith(currentToken)) {
				possibles.add(key);
			}
		}
	}
}
