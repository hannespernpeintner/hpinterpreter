package main;

import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.table.AbstractTableModel;

import datatypes.SchemeExpression;

public class StackTableModel extends AbstractTableModel {
	String[] columnNames = {"Number", "Entry"};
	private CopyOnWriteArrayList<SchemeExpression> stack;

	public CopyOnWriteArrayList<SchemeExpression> getStack() {
		return stack;
	}

	public void setStack(CopyOnWriteArrayList<SchemeExpression> stack) {
		this.stack = stack;
	}

	public StackTableModel(InterpreterGUI gui) {
		try {
			stack = gui.schemeUtility.evaluator.stack;	
		} catch (Exception e) {
			stack = new CopyOnWriteArrayList<>();
		}
	}

    public String getColumnName(int col) {
        return columnNames[col];
    }
    
	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public int getRowCount() {
		return stack.size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		if (arg1 == 0) {
			return String.valueOf(arg0);
		} else {
			return stack.get(arg0);
		}
	}

}
