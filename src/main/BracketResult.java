package main;

public class BracketResult {
	public final int first;
	public final int second;
	
	private BracketResult() {
		first = 0;
		second = 0;
	}
	
	public BracketResult(int first, int second) {
		this.first = first;
		this.second = second;
	}
	
}
