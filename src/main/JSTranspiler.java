package main;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeFloat;
import datatypes.SchemeInt;
import datatypes.SchemeList;
import datatypes.SchemeProcedure;
import datatypes.SchemeQuote;
import datatypes.SchemeString;
import datatypes.SchemeSymbol;
import datatypes.SchemeVoid;
import exceptions.InvalidParameterCountException;

public class JSTranspiler {

	public static String _float = new SchemeFloat().getPattern();
	public static String _int = new SchemeInt().getPattern();
	public static String _symbol = new SchemeSymbol().getPattern();
	public static String _string = new SchemeString().getPattern();
	
	public String transpile(SchemeExpression ex) throws InvalidParameterCountException {
		
		String token = ex.getValue();
		
		if (ex instanceof ParsedList && !(ex instanceof SchemeList)) {

			SchemeExpression first = ((ParsedList) ex).first();
			String firstTokens = "";
			try {
				firstTokens = first.getValue();
			} catch (Exception e) {
				return "";
			}
			ParsedList rest = ((ParsedList) ex).rest();
			List<String> transpiledRest = new ArrayList<String>();
			
			for (SchemeExpression schemeExpression : rest) {
				transpiledRest.add(transpile(schemeExpression));
			}
			
			if (firstTokens.startsWith("+")) {
				return StringUtils.join(transpiledRest, " + ");
				
			} else if (firstTokens.startsWith("-")) {
				return StringUtils.join(transpiledRest, " - ");
				
			} else if (firstTokens.startsWith("*")) {
				return StringUtils.join(transpiledRest, " * ");
				
			} else if (firstTokens.startsWith("/")) {
				return StringUtils.join(transpiledRest, " / ");
				
			} else if (firstTokens.startsWith("%")) {
				return StringUtils.join(transpiledRest, " % ");
				
			} else if (firstTokens.startsWith("<=")) {
				return transpiledRest.get(0) + " <= " + transpiledRest.get(1);
				
			} else if (firstTokens.startsWith(">=")) {
				return transpiledRest.get(0) + " >= " + transpiledRest.get(1);
				
			} else if (firstTokens.startsWith("<")) {
				return transpiledRest.get(0) + " < " + transpiledRest.get(1);
				
			} else if (firstTokens.startsWith(">")) {
				return transpiledRest.get(0) + " > " + transpiledRest.get(1);
				
			} else if (firstTokens.startsWith("!=")) {
				return transpiledRest.get(0) + " != " + transpiledRest.get(1);
				
			} else if (firstTokens.startsWith("=") || firstTokens.startsWith("equal")) {
				return transpiledRest.get(0) + " == " + transpiledRest.get(1);
				
			}  else if (firstTokens.startsWith("eq?")) {
				return transpiledRest.get(0) + " === " + transpiledRest.get(1);
				
			} else if (firstTokens.startsWith("max")) {
				return "Math.max(" + StringUtils.join(transpiledRest, ",") + ")";
				
			} else if (firstTokens.startsWith("min")) {
				return "Math.min(" + StringUtils.join(transpiledRest, ",") + ")";
				
			} else if (firstTokens.startsWith("list")) {
				return "[" + StringUtils.join(transpiledRest, ", ") + "]";
				
			} else if (firstTokens.startsWith("begin")) {
				return doBegin(transpiledRest);
				
			} else if (firstTokens.startsWith("nil")) {
				return "null";
				
			} else if (firstTokens.startsWith("true")) {
				return "true";
				
			} else if (firstTokens.startsWith("false")) {
				return "false";
				
			} else if (firstTokens.startsWith("return")) {
				return "return " + StringUtils.join(transpiledRest, ",") + "\n";
				
			} else if (firstTokens.startsWith("cons")) {
				return doCons(transpiledRest.get(0), StringUtils.join(transpiledRest.subList(1, transpiledRest.size()), ","));
				
			} else if (firstTokens.startsWith("first") || firstTokens.startsWith("head") || firstTokens.startsWith("car")) {
				return transpiledRest.get(0) + "[0]";
				
			} else if (firstTokens.startsWith("rest") || firstTokens.startsWith("tail") || firstTokens.startsWith("cdr")) {
				return transpiledRest.get(0) + ".slice(1, " + transpiledRest.get(0)  + ".length - 1)";
				
			} else if (firstTokens.startsWith("length")) {
				return transpiledRest.get(0) + ".length";
				
			} else if (firstTokens.startsWith("if")) {
				return "if (" + transpiledRest.get(0) + ") {\n" + transpiledRest.get(1) + "\n} else {\n" + transpiledRest.get(2) + "\n}";
				
			} else if (firstTokens.startsWith("and")) {
				return transpiledRest.get(0) + " && " + transpiledRest.get(1);
				
			} else if (firstTokens.startsWith("or")) {
				return transpiledRest.get(0) + " || " + transpiledRest.get(1);
				
			} else if (firstTokens.startsWith("define")) {
				return doDefine(rest, transpiledRest);
				
			} else if (firstTokens.startsWith("print")) {
				return "print(" + StringUtils.join(transpiledRest, ", ") + ")\n";
				
			} else if (firstTokens.startsWith("set!")) {
				return getAsParams(rest.get(0)) + " = " + transpiledRest.get(1) + ";\n";
				
			} else if (firstTokens.startsWith("lambda")) {
				//String lambdaParams = transpiledRest.get(0);
				String lambdaParams = getAsParams((ParsedList) rest.get(0));
				String lambdaBody = transpiledRest.get(1);
				return "function(" + lambdaParams + ") {\nreturn " + lambdaBody + "\n}";
				
			} else if (firstTokens.startsWith("let")) {
				if (transpiledRest.size() != 2) {
					return "";
				} else {
					return getAsParams((ParsedList) rest.get(0)) + " = " + getAsParams((ParsedList) rest.get(1)) + ";\n";
				}
				
			} else if (firstTokens.startsWith("quote")) {
				return firstTokens;
			} else if (firstTokens.matches(_symbol)) {
				List<String> symbols = new ArrayList<>();
				String result = firstTokens;
				symbols.addAll(transpiledRest);
				result += "(" + StringUtils.join(symbols, ", ") + ")";
				return result;
			} else {
				List<String> symbols = new ArrayList<>();
				symbols.add(firstTokens);
				symbols.addAll(transpiledRest);
				return StringUtils.join(symbols, ", ");
			}
			
		} else if (token.matches(_float)) {
			return token;
		} else if (token.matches(_int)) {
			return token;
		}  else if (token.startsWith("nil")) {
			return "null";
		}	
		else if (token.matches(_string)) {
			return token;
		} else if (ex instanceof SchemeVoid || token == "void") {
			return token;
		} else if (token.matches(_symbol)) {
			return token;
		} else if (ex instanceof SchemeQuote) {
			return token;
		}
		return token;
	}
	
	private String doCons(String firstElem, String array) {
		String result = array.substring(0,1);
		result += firstElem + ", " + array.substring(1);
		
		return result;
	}

	private String doBegin(List<String> params) {
		String result = StringUtils.join(params, "\n");
//		String result = "{ " + StringUtils.join(params, "}; ");
		return result + "\n";
	}
	private String doDefine(ParsedList params, List<String> transpiledRest) {
		try {
			if (params.get(0) instanceof ParsedList && ((ParsedList) params.get(0)).size() > 1) {
				ParsedList lambdaAndParams = (ParsedList) params.get(0);
				ParsedList lambdaBody = (ParsedList) params.get(1);
				String lambdaBodyString = transpile(lambdaBody);
				List<SchemeExpression> lambdaParams = lambdaAndParams.getList().subList(1, lambdaAndParams.size());
				String lambdaParamsString = getAsParams(lambdaParams);
				return "var " + lambdaAndParams.get(0) + " = function(" + lambdaParamsString + ") {\n\t" + lambdaBodyString + "\n};\n";
			} else {
				String variable = getAsParams(params.get(0));
				return "var " + variable + " = " + transpiledRest.get(1) + ";\n";
			}
		} catch (InvalidParameterCountException e) {
			e.printStackTrace();
		}
		return "";
	}


	public String getAsParams(ParsedList params) {
		String result = "";
		result += StringUtils.join(params, ", ");
		return result + "";
	}
	public String getAsParams(List params) {
		String result = "";
		result += StringUtils.join(params, ", ");
		return result + "";
	}
	private String getAsParams(SchemeExpression param) {
		String result = StringUtils.replace(param.toString(), "(", "");
		result = StringUtils.replace(result, ")", "");
		return result;
	}
}
