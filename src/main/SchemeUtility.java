package main;

import java.util.Date;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTree;

import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.DefaultCompletionProvider;

import datatypes.Environment;
import datatypes.ParsedList;
import datatypes.SchemeExpression;
import exceptions.InvalidParameterCountException;
import exceptions.ReservedKeyWordException;
import exceptions.SymbolException;

public class SchemeUtility {

	private InterpreterGUI gui = null;
	private static SchemeUtility uniqInstance;
	public static final String[] keywords = {"+", "-", "*", "//", "%", "<=", ">=", "<", ">", "!=", "=", "eq?", "max", "min", "list", "nil", "true", "false", "cons", "first", "head", "car",
		"rest", "tail", "cdr", "length", "if", "define", "print", "set!", "lambda", "let", "quote", "begin", "and", "or", "void"};
	public final SchemeParser parser = new SchemeParser();
	public final SchemeEvaluator evaluator = new SchemeEvaluator();
	public Environment globalEnvironment = new Environment();

	private boolean isDebug = false;
	private boolean withStack = false;

	public static synchronized SchemeUtility getInstance() {
	    if (uniqInstance == null) {
	      uniqInstance = new SchemeUtility();
	    }
	    return uniqInstance;
	}
	
	private SchemeUtility() {
	}

	public ParsedList parse(String input) {
		if (input == null) {
			input = "";
		}
		String cleanedInput = clean(input);
		return parser.parseAll(cleanedInput);
	}
	

	public SchemeExpression evaluate(SchemeExpression input) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		return evaluate(input, globalEnvironment);
	}

	public SchemeExpression evaluate(SchemeExpression input, Environment env) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		return evaluator.evaluate(input, env);

	}

	public SchemeExpression interpret(String input) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		evaluator.stack = new CopyOnWriteArrayList<>();
		SchemeExpression result = evaluate(parse(input));
		//System.out.println(evaluator.stack);
		return result;
	}
	
	public SchemeExpression interpretAndReset(String input) throws SymbolException, InvalidParameterCountException, ReservedKeyWordException {
		SchemeExpression result = interpret(input);
		resetEnvironment();
		return result;
	}
	
	private String clean(String input) {
		input = input.replaceAll(Pattern.quote("'("), "(list "); // Syntactic sugar for quote builtin function

//		String quote = Pattern.quote("'[^\\s]+");
//		Pattern pattern = Pattern.compile("'[^\\s]+");
//        Matcher matcher = pattern.matcher(input);
//        while(matcher.find())
//        {
//            String find = matcher.group();
//            find = find.replaceFirst("'", "");
//            input.replaceFirst(find, "(quote " + find + ")");
//        }
		
		return input;
	}
	
	public JTree convert(ParsedList result) {
		return parser.convert(result);
	}

	public void resetEnvironment() {
		globalEnvironment = new Environment();
		//evaluator.stack = new ArrayList<>();
		evaluator.stack = new CopyOnWriteArrayList<>();
//		try {
//			globalEnvironment.define("nil", new SchemeList());
//		} catch (ReservedKeyWordException | SymbolException e) {
//			e.printStackTrace();
//		}
	}

	public boolean isDebug() {
		return isDebug;
	}

	public void setDebug(boolean isDebug) {
		this.isDebug = isDebug;
	}

	public void debug(String string) {
		if (isDebug) {
			System.out.println(new Date() + " - " + string);
		}
	}
	
	public BracketResult getCorrespondingBracket(String input, int startPos) {
		return parser.findCorrespondingBracket(input, startPos);
	}

	public void addAutoCompletion(String key) {
		if (evaluator.gui != null) {
			DefaultCompletionProvider provider = evaluator.gui.getProvider();
			provider.addCompletion(new BasicCompletion(provider, key));
		}
	}

	public boolean withStack() {
		return withStack ;
	}
	
	public void setWithStack(boolean b) {
		withStack = b;
	}
}
