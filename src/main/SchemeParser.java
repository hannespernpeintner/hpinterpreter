package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import datatypes.ParsedList;
import datatypes.SchemeExpression;
import datatypes.SchemeList;
import exceptions.SyntaxErrorException;


public class SchemeParser {

	public static String EXPRESSIONSTART = "(";
	public static String EXPRESSIONEND = ")";

	public char[] charToNextToken = {' ', ')', '('};
	public char[] charBrackets = {')', '('};

	List<String> tokens = new ArrayList<>();
	List<Integer> lineNumbers = new ArrayList<>();
	List<Integer> positions = new ArrayList<>();
	boolean inComment = false;
	boolean tokenStarted = false;
	
	List<List> tokenListLineNumberListPositionList = new ArrayList<>();
	String current = "";
	int currentLine = 0;
	int currentPosition = 0;
	int tokenStartPosition = 0;
	
	public SchemeParser() {
		
	}
	
	public String removeFirstOuterBrackets (String input) {
		int firstOpenBracketIndex = input.indexOf(EXPRESSIONSTART);
		int lastCloseBracketIndex = input.lastIndexOf(EXPRESSIONEND);
		if (firstOpenBracketIndex >= 0) {
			return input.substring(firstOpenBracketIndex+1, lastCloseBracketIndex);	
		} else {
			return input;
		}
	}

	public ParsedList parseAll(String input) {

		ParsedList result = new ParsedList();
		try {
			checkParanthesis(input);
		
			List<List> threeLists = tokenize(input);
			//this.tokens = tokenize(input);
			this.tokens = threeLists.get(0);
			this.lineNumbers = threeLists.get(1);
			this.positions = threeLists.get(2);
	
			//parse(tokens, false, result);
			parse(tokens, lineNumbers, positions, false, result);

			return  (ParsedList) result.getList().get(0);
		} catch (SyntaxErrorException e) {
			return SchemeExpression.NIL();
		}
	}
	
	
	private ParsedList parse(final List<String> tokens, final List<Integer> lineNumbers, List<Integer> positions, boolean inner, ParsedList result) throws SyntaxErrorException {

        Integer currentLineNumber = lineNumbers.get(0);
        Integer currentPosition = positions.get(0);
        while (tokens.size() > 0) {
            String current = tokens.get(0);
            tokens.remove(0);
            lineNumbers.remove(0);
            positions.remove(0);
            
            if ("(".equals(current)) {
            	
                result.add(parse(tokens, lineNumbers, positions, true, new ParsedList()));
                
            } else if (")".equals(current)) {

            	if( inner) {
            		
            		return result;
            		
            	} else {
            		
                    throw new SyntaxErrorException("Unmatched close parent", currentLineNumber, currentPosition);
            	}
            	
            } else {
            	
            	result.add(new SchemeExpression(current, currentLineNumber, currentPosition));
            }
        }
        
        if (inner) {
            throw new SyntaxErrorException("Unmatched open parent", currentLineNumber, currentPosition);
        } else {

            return result;
        }
	}
	
	//public List<String> tokenize(String input) {
	public List<List> tokenize(String input) {
		clearCounters();
		
		outerloop:
		for (int i = 0; i < input.length(); i++, currentPosition++) {
			char c = input.charAt(i);
			
			if (c == ' ' && !inComment) {
				
				if (current.length() > 0) {

					addTokenLineNumberPosition(current, currentLine, tokenStartPosition);
					resetCurrentToken();
				}
				
			} else if (('(' == c || ')' == c) && !inComment) {
				
				if(current.length() > 0) {

					addTokenLineNumberPosition(current, currentLine, tokenStartPosition);
					resetCurrentToken();
				}
				addTokenLineNumberPosition(String.valueOf(c), currentLine, currentPosition);
				
			} else if (';' == c && !inComment) {
				if(current.length() > 0) {

					addTokenLineNumberPosition(current, currentLine, tokenStartPosition);
					resetCurrentToken();
				}
				inComment = true;
				
			} else if ('\r' == c) {
				if (current.length() > 0 && !inComment) {

					addTokenLineNumberPosition(current, currentLine, tokenStartPosition);
				}
				resetCurrentToken();
				currentLine += 1;
				currentPosition = -1;
				inComment = false;
				
			} else if ('\n' == c) {
				if (current.length() > 0 && !inComment) {

					addTokenLineNumberPosition(current, currentLine, tokenStartPosition);
					
				}
				resetCurrentToken();
				currentLine += 1;
				currentPosition = -1;
				inComment = false;
				
			} else {
				startToken();
				current = current + String.valueOf(c);
				
			}
		}
		
		if (current.length() > 0 && !inComment) {

			addTokenLineNumberPosition(current, currentLine, tokenStartPosition);
		}

		tokenListLineNumberListPositionList.add(tokens);
		tokenListLineNumberListPositionList.add(lineNumbers);
		tokenListLineNumberListPositionList.add(positions);
		//return tokens;
		return tokenListLineNumberListPositionList;
	}
	
	private void startToken() {
		if (!tokenStarted) {
			tokenStarted = true;
			tokenStartPosition = currentPosition;
		}
	}
	
	private void endToken() {
		tokenStarted = false;
	}

	private void clearCounters() {
		tokens = new ArrayList<>();
		lineNumbers = new ArrayList<>();
		positions = new ArrayList<>();
		inComment = false;
		
		tokenListLineNumberListPositionList = new ArrayList<>();
		current = "";
		currentLine = 0;
		currentPosition = 0;
		tokenStartPosition = 0;
	}

	private void addTokenLineNumberPosition(String token, int lineNumber, int position) {
		if (!inComment) {
			tokens.add(token);
			lineNumbers.add(lineNumber);
			positions.add(position);
		}
	}

	private void resetCurrentToken() {
		current = "";
		endToken();
	}
	
	public boolean checkParanthesis(String input) throws SyntaxErrorException {
		boolean check = startsAndEndsWithParanthesis(input);
//		boolean check = true;
		if (!check) {
			throw new SyntaxErrorException("expression is not starting and ending with paranthesis: " + input, new Integer(0), new Integer(0));
		}
		
		check = paranthesIsBalanced(input);
		if (!check) {
			throw new SyntaxErrorException("unequal count of brackets in: " + input, new Integer(0), new Integer(0));
		}
		
		return check;
	}
	
	private boolean startsAndEndsWithParanthesis(String input) {
		String tempInput = input.replaceAll("[\\r]", "");
		tempInput = tempInput.replaceAll("[\\n]", "");
		return (tempInput.startsWith(EXPRESSIONSTART) && tempInput.endsWith(EXPRESSIONEND)) || tempInput.startsWith("'");
	}
	
	private boolean paranthesIsBalanced (String input) throws SyntaxErrorException {
		try {
			int countStart = countOccurrences(input, '(');
			int countEnd = countOccurrences(input, ')');
			if (countStart != countEnd) {
				return (countStart == countEnd);
			}
		} catch (PatternSyntaxException e) {
			throw new SyntaxErrorException(input, new Integer(0), new Integer(0));
		}
		return true;
	}
	
	public int findNextOccurence(String haystack, char[] needles) {
		Character[] a = convert(needles);
		Integer[] b = new Integer[needles.length];
		
		for (int i = 0; i < needles.length; i++) {
			b[i] = haystack.indexOf(String.valueOf(needles[i]));
		}
		int lowest = Collections.max(Arrays.asList(b));
		
		for (int i = 0; i < b.length; i++) {
			if (b[i] < lowest && b[i] >= 0) {
				lowest = b[i];
			}
		}
		
		return lowest;
	}
	
	public int findNextOccurence(String haystack, char needle) {
		int pos = haystack.indexOf(String.valueOf(needle));
		
		if (pos <0) {
			return 0;
		}
		
		return pos;
	}
	
	public int countOccurrences(String haystack, char needle)
	{
	    int count = 0;
	    for (int i=0; i < haystack.length(); i++)
	    {
	        if (haystack.charAt(i) == needle)
	        {
	             count++;
	        }
	    }
	    return count;
	}
	
	public Character[] convert(char[] chars) {
        Character[] copy = new Character[chars.length];
        for(int i = 0; i < copy.length; i++) {
            copy[i] = Character.valueOf(chars[i]);
        }
        return copy;
    }

	public JTree convert(ParsedList input) {
		
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root");
		addAllChildren(input, root);
		
		JTree tree = new JTree(root);
		return tree;
		
	}
	
	private void addAllChildren(ParsedList input, DefaultMutableTreeNode target) {
		for (int i = 0; i < input.getList().size(); i++) {
			SchemeExpression ex = input.getList().get(i);
			target.add(ex);
			if (ex instanceof SchemeList) {
				addAllChildren(((SchemeList) ex), ex);
			}
		}
	}


	public BracketResult findCorrespondingBracket(String input){
		BracketResult res = findCorrespondingBracket(input, 0);
		if (res.first >= 0 && res.second >= 0) {
			return res;
		}
		return new BracketResult(0, 0);
	}
	
	public BracketResult findCorrespondingBracket(String input, int startPos){
		
		int cursor = startPos;
		if (cursor == input.length()) {
			cursor--;
		}
		
		char currentToken = input.charAt(cursor);
		
		if (currentToken == ')') {
			return new BracketResult(cursor, helperFindCorrespondingBracket(input, cursor, '(', true));
			
		} else if (currentToken == '(') {
			return new BracketResult(cursor, helperFindCorrespondingBracket(input, cursor, ')', false));
		} else {
			if (cursor >= 0) {
				cursor--;
				currentToken = input.charAt(cursor);
				if (currentToken == ')') {
					return new BracketResult(cursor, helperFindCorrespondingBracket(input, cursor, '(', true));
					
				} else if (currentToken == '(') {
					return new BracketResult(cursor, helperFindCorrespondingBracket(input, cursor, ')', false));
					
				}
			}
		}

		return null;
	}
	
	private int helperFindCorrespondingBracket(String input, int startPos, char toSearch, boolean backwards) {
		int openBracketCounter = 0;
		int closeBracketCounter = 0;
		int result = -1;

		if (backwards) {
			int cursor = startPos;
			while(cursor >= 0) {
				char current = input.charAt(cursor);
				if ('(' == current) {
					openBracketCounter++;
				} else if (')' == current) {
					closeBracketCounter++;
				}
				if (toSearch == current && closeBracketCounter == openBracketCounter) {
					return cursor;
				}
				cursor--;
			}
		} else {
			int cursor = startPos;
			while(cursor < input.length()) {
				char current = input.charAt(cursor);
				if ('(' == current) {
					openBracketCounter++;
				} else if (')' == current) {
					closeBracketCounter++;
				}
				if (toSearch == current && closeBracketCounter == openBracketCounter) {
					return cursor;
				}
				cursor++;
			}
		}
		return -1;
	}
	

}
