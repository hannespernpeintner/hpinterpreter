package main;

import java.awt.Color;

import javax.swing.text.DefaultHighlighter;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;

public class TextHighlighter {
	
	public DefaultHighlightPainter highliter =  new DefaultHighlighter.DefaultHighlightPainter(Color.LIGHT_GRAY);
	public String pattern = "xxx";

	public TextHighlighter(String pattern, Color color) {
		highliter = new DefaultHighlighter.DefaultHighlightPainter(color);
		this.pattern = pattern;
	}


}
