package exceptions;

public class SyntaxErrorException extends Throwable  {

	public SyntaxErrorException(String input, Integer lineNumber, Integer currentPosition) {
		System.err.println("LINE " +lineNumber+1 + " POS " +currentPosition + ": " + "SyntaxErrorException because of " + input);
	}
}
