package exceptions;

import datatypes.SchemeExpression;

public class InvalidParameterCountException extends Throwable {
	
	protected String subject;
	protected int expected;
	protected int given;
	Integer lineNumber;
	Integer position;

	public InvalidParameterCountException(String subject, SchemeExpression expr, int expected, int given) {
		this.subject = subject;
		this.expected = expected;
		this.given = given;
		this.lineNumber = expr.getLineNumber();
		this.position = expr.getPosition();
	}

	@Override
	public String getMessage() {
		return "LINE " +(lineNumber+1) + " POS " + position + ": " + subject + " expected " + expected + " parameters, but " + given + " is/are given";
	}
}
