package exceptions;

import datatypes.SchemeExpression;

public class InvalidIndexException extends InvalidParameterCountException {

	public InvalidIndexException(String subject, SchemeExpression expr, int expected, int given) {
		super(subject, expr, expected, given);
	}
	
	@Override
	public String getMessage() {
		return "LINE " +lineNumber+1 + " POS "+ position +": " + "Invalid index access on " + subject + ": tried to access index " + this.expected + " but only " + given + " given.";
	}

}
