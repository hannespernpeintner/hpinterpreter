package exceptions;

@SuppressWarnings("serial")
public class SymbolNotDefinedException extends Throwable {
	
	public enum LookupType {
		Lookup,
		Update
	}
	
	private String symbolName = "";
	private LookupType lookupType;
	
	public SymbolNotDefinedException(String symbolName, LookupType lookupType) {
		this.symbolName = symbolName;
		this.setLookupType(lookupType);
	}
	
	public String getSymbolName() {
		return symbolName;
	}

	public void setSymbolName(String symbolName) {
		this.symbolName = symbolName;
	}

	public LookupType getLookupType() {
		return lookupType;
	}

	public void setLookupType(LookupType lookupType) {
		this.lookupType = lookupType;
	}
	
	@Override
	public String getMessage() {
		String text = "updated";
		if (lookupType == LookupType.Lookup) {
			text = "looked up";
		}
		return "Symbol " + symbolName + " can't be " + text + ", because it isn't defined";
	}
}
