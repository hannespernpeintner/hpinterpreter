package exceptions;

public class InvalidDataTypeException extends RuntimeException {
	
	String subject;
	String expected;
	String given;
	Integer lineNumber;
	Integer position;
	
	public InvalidDataTypeException(String subject, Integer lineNumber, Integer position, String expectedCommaSeperated, String given) {
		this.subject = subject;
		this.expected = expectedCommaSeperated;
		this.given = given;
		this.lineNumber = lineNumber;
		this.position = position;
	}

	@Override
	public String getMessage() {
		return "LINE " +(lineNumber+1) + " POS " + position + ": " + subject + " expected " + expected + " instead of " + given;
	}
	@Override
	public String toString() {
		return "LINE " +(lineNumber+1) + " POS " + position + ": " + subject + " expected " + expected + " instead of " + given;
	}
	
}
