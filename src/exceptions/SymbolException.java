package exceptions;

@SuppressWarnings("serial")
public class SymbolException extends RuntimeException {
	
	public enum LookupType {
		Define,
		Lookup,
		Update
	}
	
	private String symbolName = "";
	private LookupType lookupType;
	private Integer lineNumber;
	
	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	public SymbolException(String symbolName, Integer lineNumber, LookupType lookupType) {
		this.symbolName = symbolName;
		this.setLookupType(lookupType);
		this.lineNumber = lineNumber;
	}
	
	public SymbolException() {
	}

	public String getSymbolName() {
		return symbolName;
	}

	public void setSymbolName(String symbolName) {
		this.symbolName = symbolName;
	}

	public LookupType getLookupType() {
		return lookupType;
	}

	public void setLookupType(LookupType lookupType) {
		this.lookupType = lookupType;
	}

	@Override
	public String getMessage() {
		String text = "updated, because it isn't defined";
		if (lookupType == LookupType.Lookup) {
			text = "looked up, because it isn't defined";
		} else if (lookupType == LookupType.Define) {
			text = "defined, because it is already";
		}
		return "LINE " +(lineNumber+1) + ": " + "Symbol " + symbolName + " can't be " + text;
	}

	@Override
	public String toString() {
		String text = "updated, because it isn't defined";
		if (lookupType == LookupType.Lookup) {
			text = "looked up, because it isn't defined";
		} else if (lookupType == LookupType.Define) {
			text = "defined, because it is already";
		}
		return "LINE " +(lineNumber+1) + ": " + "Symbol " + symbolName + " can't be " + text;
	}
}
