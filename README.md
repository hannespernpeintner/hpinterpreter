# HPInterpreter
### A Lisp interpreter library with a very comprehensive IDE

---

## Features

* Can interpret **Lisp** code
* Regular or **single-step** mode (for debugging for example)
* Can **compile to JavaScript** (However, this feature is not completed yet, so check the putput)
* Can **run JavaScript** afterwards
* Several other **debug** functions... verbose output, stack printing, AST-view, live environment manipulation
* Rhich Editor with intelligent Syntax Highlighting, selection execution, shortcuts etc.
* Errors with line numbers and curret location

## Usage options

* Download and **execute jar** from the download section
* Download jar and batch file from the download section. Place them in one folder and **run batch** file to use a larger stack size.
Modify batch file as you want or use it as a shell script on **linux**.
* Download the project and use it as an eclipse project. There are **more than 100 unit and functional tests** in the test package, ready to be run with JUnit.

## Interpreter core

The interpreter is pretty much an in-out-system. Means you put in your Lisp code and it returns your result. Examples:

```js
=>(+ 10 10 10 10 10)
=> 50 // calls the built in plus with 5 times 10 as parameters and returns the sum
```
```js
=>((lambda (a b c) (+ a b c)) 1 1 1)
=> 3 // uses a lambda to compute the sum
```
```js
=>(define (proc param1 param2) (+ param1 param2))
=>(param1 param2) - (+ param1 param2) // Returned the definement: A procedure that takes two params and does a plus operation
```

Shorthand syntaxes are provided, for example for quote:

```js
=>(print ((quote "sdfsdf")))
=> "sdfsdf"
=>(print ('("sdfsdf")))
=> "sdfsdf"
```

Line comments are done with ";"

All built in commands:

**+, -, *, /, %, <=, >=, <, >, !=, =, eq?, max, min, list, nil, cons, first, head, car, rest, tail, cdr, length, if, define, print, set!, lambda, let, quote, begin, and, or, void**

For further information, here's a register of built in commands:

[All built in commands with examples and explanation](https://bitbucket.org/hannespernpeintner/hpinterpreter/raw/eaeee1c372a70c9df346cb7d7dbee95681f9b607/img/interpreter_commands.html)


## Datatypes

These are the built in datatypes

* **Boolean**
```js
=>(define a true)
=> true
=> (a)
=> true
```

* **Integer**
```js
=>(define a 10)
=> 10
=> (a)
=> 10
```

* **Float**
```js
=>(define a 10.0)
=> 10.0
=> (a)
=> 10.0
```

* **String**
```js
=> (define a "10")
=> "10"
=> (a)
=> "10"
```

* **Symbol**
```js
=> (define a 10)
=> (define b a)
=> (b)
=> 10
```

* **Void** (Some functions return void, like the let instruction. Nevermind.)

* **Quote** (Has its own datetype, but shouldn't be interesting for you.)

* **Procedure**
```js
=>(lambda (a b c) (+ a b c))
=> (a b c) - (+ a b c) // String representation of a procedure
```

* **List**
```js
=> (list 1 2 3)
=> (1 2 3)
```


## IDE

![alt text](https://bitbucket.org/hannespernpeintner/hpinterpreter/raw/91d86c2864010cdd6827600cd01801f5a16e9870/img/HPInterpreter_example.PNG "Logo Title Text 1")

The IDE is just a comprehensive interface for the Lisp interpreter. The interpreter itself is stateless. However, the wrapping utility used by the IDE, is not. It uses a global environment which you have to reset if you want to interpret from scratch.

Using the command bar and the editor/console views should be self explaining, once you've opened the program.

* All **input goes in the white editor view**. There's no command line - the black view below the editor is just a console, that prints your results.
* Autocompletion is used with STRG + Space

## Command bar

![alt text](https://bitbucket.org/hannespernpeintner/hpinterpreter/raw/eaeee1c372a70c9df346cb7d7dbee95681f9b607/img/Play%20Green%20Button.png "Logo Title Text 1") Runs all the code in the Lisp editor view

![alt text](https://bitbucket.org/hannespernpeintner/hpinterpreter/raw/eaeee1c372a70c9df346cb7d7dbee95681f9b607/img/Play%20Blue%20Button.png "Logo Title Text 1") Runs all selected code in the Lisp editor view
Alternatively, you can use right click context menu in the editor to run selection.

![alt text](https://bitbucket.org/hannespernpeintner/hpinterpreter/raw/eaeee1c372a70c9df346cb7d7dbee95681f9b607/img/Play%20All.png "Logo Title Text 1") _[Only in single-step mode]_ Resume the execution, which means switching back to regular mode, because breakpoints arent' yet implemented.

![alt text](https://bitbucket.org/hannespernpeintner/hpinterpreter/raw/eaeee1c372a70c9df346cb7d7dbee95681f9b607/img/Play.png "Logo Title Text 1") _[Only in single-step mode]_ Perform a single execution step, which means the evaluation of the next expression.

![alt text](https://bitbucket.org/hannespernpeintner/hpinterpreter/raw/eaeee1c372a70c9df346cb7d7dbee95681f9b607/img/Help%20Blue%20Button.png "Logo Title Text 1") _[Only in single-step mode]_ Opens a documentation. It's incomplete, use this page for reference.

### File
Can be used to open files. Alternatively, you can drag and drop any files into the editor view to open them.

### Code
Offers command buttons for evaluation, compilation, environment resetting and stuff. Shortcuts can be found there.

### Console
Lets you activate debug stuff and clear the console. Shortcuts included.

### Singlestep Toggle
Lets you chose between singlestep and regular mode. In singlestep, you can do a single execution step or resume the execution (see buttons above). Your current environment and expression view is updated on every single step.

### Implicit begin wrapping
Wraps the editor content with a begin statement. Only the top level is wrapped!

### Thread state bar
Shows you wether an execution is currently running or not. States if the execution is awaiting a step command, if singlestep is active.

## Additional views

### AST
Offers you a tree view of the Abstract Snytax Tree of your code. No life parsing, only if you do a parse/interprete command.

### Stack
Shows the complete execution stack that is updated with every execution step. Use this with care, because it slows down the execution heavily.

### Global environment
Shows you key-value-pairs of your execution environment. To manipulate your already defined data, just type in the value field. Your input is interpreted directly afterwards.

### JavaScript
Shows your code in JavaScript after you compiled it. Mind that this is beta, so this code might not be correct.